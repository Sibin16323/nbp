﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class Form1 : Form
    {

        private GraphClient client;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "sibin");
            try
            {
                client.Connect();
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void btnKorisnici_Click(object sender, EventArgs e)
        {
            FormKorisnici KorisniciForm = new FormKorisnici();
            KorisniciForm.client = client;
            KorisniciForm.ShowDialog();
        }

        private void btnLicneKarte_Click(object sender, EventArgs e)
        {
            FormLicneKarte LicneKarteForm = new FormLicneKarte();
            LicneKarteForm.client = client;
            LicneKarteForm.ShowDialog();
        }

        private void btnPasosi_Click(object sender, EventArgs e)
        {
            FormPasosi PasosiForm = new FormPasosi();
            PasosiForm.client = client;
            PasosiForm.ShowDialog();
        }

        private void btnRegistracijaVozila_Click(object sender, EventArgs e)
        {
            FormRegistracijeVozila RegistracijeVozilaForm = new FormRegistracijeVozila();
            RegistracijeVozilaForm.client = client;
            RegistracijeVozilaForm.ShowDialog();
        }

        private void btnSaobracajneKazne_Click(object sender, EventArgs e)
        {
            FormSaobracajneKazne SaobracajneKazneForm = new FormSaobracajneKazne();
            SaobracajneKazneForm.client = client;
            SaobracajneKazneForm.ShowDialog();
        }

        private void btnVozackeDozvole_Click(object sender, EventArgs e)
        {
            FormVozackeDozvole VozackeDozvoleForm = new FormVozackeDozvole();
            VozackeDozvoleForm.client = client;
            VozackeDozvoleForm.ShowDialog();
        }

        private void btnRadnici_Click(object sender, EventArgs e)
        {
            FormRadnici RadniciForm = new FormRadnici();
            RadniciForm.client = client;
            RadniciForm.ShowDialog();
        }
    }
}
