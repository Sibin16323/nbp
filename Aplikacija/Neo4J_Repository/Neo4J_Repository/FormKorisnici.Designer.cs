﻿namespace Neo4J_Repository
{
    partial class FormKorisnici
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaLK = new System.Windows.Forms.Button();
            this.txbIme = new System.Windows.Forms.TextBox();
            this.btnSaP = new System.Windows.Forms.Button();
            this.btnSaRV = new System.Windows.Forms.Button();
            this.btnSaSK = new System.Windows.Forms.Button();
            this.btnSaVD = new System.Windows.Forms.Button();
            this.btnBezVD = new System.Windows.Forms.Button();
            this.btnBezSK = new System.Windows.Forms.Button();
            this.btnBezRV = new System.Windows.Forms.Button();
            this.btnBezP = new System.Windows.Forms.Button();
            this.btnBezLK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.korisnici = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txbJmbg = new System.Windows.Forms.TextBox();
            this.txbMestoRodjenja = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnPrikaziVozacku = new System.Windows.Forms.Button();
            this.btnPrikaziKazne = new System.Windows.Forms.Button();
            this.btnPrikaziRegistracije = new System.Windows.Forms.Button();
            this.btnPrikaziPasos = new System.Windows.Forms.Button();
            this.btnPrikaziLicnuKartu = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnPoMestuRodjenja = new System.Windows.Forms.Button();
            this.btnPoJMBG = new System.Windows.Forms.Button();
            this.btnPoImenu = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSaLK
            // 
            this.btnSaLK.Location = new System.Drawing.Point(21, 28);
            this.btnSaLK.Name = "btnSaLK";
            this.btnSaLK.Size = new System.Drawing.Size(167, 52);
            this.btnSaLK.TabIndex = 0;
            this.btnSaLK.Text = "Svi korisnici sa licnim kartama";
            this.btnSaLK.UseVisualStyleBackColor = true;
            this.btnSaLK.Click += new System.EventHandler(this.btnSaLK_Click);
            // 
            // txbIme
            // 
            this.txbIme.Location = new System.Drawing.Point(16, 21);
            this.txbIme.Name = "txbIme";
            this.txbIme.Size = new System.Drawing.Size(149, 22);
            this.txbIme.TabIndex = 0;
            // 
            // btnSaP
            // 
            this.btnSaP.Location = new System.Drawing.Point(218, 28);
            this.btnSaP.Name = "btnSaP";
            this.btnSaP.Size = new System.Drawing.Size(167, 52);
            this.btnSaP.TabIndex = 1;
            this.btnSaP.Text = "Svi korisnici sa pasosima";
            this.btnSaP.UseVisualStyleBackColor = true;
            this.btnSaP.Click += new System.EventHandler(this.btnSaP_Click);
            // 
            // btnSaRV
            // 
            this.btnSaRV.Location = new System.Drawing.Point(421, 28);
            this.btnSaRV.Name = "btnSaRV";
            this.btnSaRV.Size = new System.Drawing.Size(167, 52);
            this.btnSaRV.TabIndex = 2;
            this.btnSaRV.Text = "Svi korisnici sa registrovanim vozilima";
            this.btnSaRV.UseVisualStyleBackColor = true;
            this.btnSaRV.Click += new System.EventHandler(this.btnSaRV_Click);
            // 
            // btnSaSK
            // 
            this.btnSaSK.Location = new System.Drawing.Point(620, 28);
            this.btnSaSK.Name = "btnSaSK";
            this.btnSaSK.Size = new System.Drawing.Size(167, 52);
            this.btnSaSK.TabIndex = 3;
            this.btnSaSK.Text = "Svi korisnici sa saobracajnim kaznama";
            this.btnSaSK.UseVisualStyleBackColor = true;
            this.btnSaSK.Click += new System.EventHandler(this.btnSaSK_Click);
            // 
            // btnSaVD
            // 
            this.btnSaVD.Location = new System.Drawing.Point(824, 28);
            this.btnSaVD.Name = "btnSaVD";
            this.btnSaVD.Size = new System.Drawing.Size(167, 52);
            this.btnSaVD.TabIndex = 4;
            this.btnSaVD.Text = "Svi korisnici sa vozackim dozvolama";
            this.btnSaVD.UseVisualStyleBackColor = true;
            this.btnSaVD.Click += new System.EventHandler(this.btnSaVD_Click);
            // 
            // btnBezVD
            // 
            this.btnBezVD.Location = new System.Drawing.Point(824, 107);
            this.btnBezVD.Name = "btnBezVD";
            this.btnBezVD.Size = new System.Drawing.Size(167, 52);
            this.btnBezVD.TabIndex = 9;
            this.btnBezVD.Text = "Svi korisnici bez vozackih dozvola";
            this.btnBezVD.UseVisualStyleBackColor = true;
            this.btnBezVD.Click += new System.EventHandler(this.btnBezVD_Click);
            // 
            // btnBezSK
            // 
            this.btnBezSK.Location = new System.Drawing.Point(620, 107);
            this.btnBezSK.Name = "btnBezSK";
            this.btnBezSK.Size = new System.Drawing.Size(167, 52);
            this.btnBezSK.TabIndex = 8;
            this.btnBezSK.Text = "Svi korisnici bez saobracajnih kazni";
            this.btnBezSK.UseVisualStyleBackColor = true;
            this.btnBezSK.Click += new System.EventHandler(this.btnBezSK_Click);
            // 
            // btnBezRV
            // 
            this.btnBezRV.Location = new System.Drawing.Point(421, 107);
            this.btnBezRV.Name = "btnBezRV";
            this.btnBezRV.Size = new System.Drawing.Size(167, 52);
            this.btnBezRV.TabIndex = 7;
            this.btnBezRV.Text = "Svi korisnici bez registrovanih vozila";
            this.btnBezRV.UseVisualStyleBackColor = true;
            this.btnBezRV.Click += new System.EventHandler(this.btnBezRV_Click);
            // 
            // btnBezP
            // 
            this.btnBezP.Location = new System.Drawing.Point(218, 107);
            this.btnBezP.Name = "btnBezP";
            this.btnBezP.Size = new System.Drawing.Size(167, 52);
            this.btnBezP.TabIndex = 6;
            this.btnBezP.Text = "Svi korisnici bez pasosa";
            this.btnBezP.UseVisualStyleBackColor = true;
            this.btnBezP.Click += new System.EventHandler(this.btnBezP_Click);
            // 
            // btnBezLK
            // 
            this.btnBezLK.Location = new System.Drawing.Point(21, 107);
            this.btnBezLK.Name = "btnBezLK";
            this.btnBezLK.Size = new System.Drawing.Size(167, 52);
            this.btnBezLK.TabIndex = 5;
            this.btnBezLK.Text = "Svi korisnici bez licnih karata";
            this.btnBezLK.UseVisualStyleBackColor = true;
            this.btnBezLK.Click += new System.EventHandler(this.btnBezLK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBezVD);
            this.groupBox1.Controls.Add(this.btnSaLK);
            this.groupBox1.Controls.Add(this.btnBezSK);
            this.groupBox1.Controls.Add(this.btnSaP);
            this.groupBox1.Controls.Add(this.btnBezRV);
            this.groupBox1.Controls.Add(this.btnSaRV);
            this.groupBox1.Controls.Add(this.btnBezP);
            this.groupBox1.Controls.Add(this.btnSaSK);
            this.groupBox1.Controls.Add(this.btnBezLK);
            this.groupBox1.Controls.Add(this.btnSaVD);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1014, 177);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.korisnici);
            this.groupBox2.Location = new System.Drawing.Point(13, 196);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1013, 196);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista korisnika";
            // 
            // korisnici
            // 
            this.korisnici.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.korisnici.Dock = System.Windows.Forms.DockStyle.Fill;
            this.korisnici.FullRowSelect = true;
            this.korisnici.GridLines = true;
            this.korisnici.Location = new System.Drawing.Point(4, 19);
            this.korisnici.Margin = new System.Windows.Forms.Padding(4);
            this.korisnici.Name = "korisnici";
            this.korisnici.Size = new System.Drawing.Size(1005, 173);
            this.korisnici.TabIndex = 1;
            this.korisnici.UseCompatibleStateImageBehavior = false;
            this.korisnici.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Ime";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Prezime";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 120;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "JMBG";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 120;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Datum rodjenja";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 125;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Mesto rodjenja";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 125;
            // 
            // txbJmbg
            // 
            this.txbJmbg.Location = new System.Drawing.Point(16, 64);
            this.txbJmbg.Name = "txbJmbg";
            this.txbJmbg.Size = new System.Drawing.Size(149, 22);
            this.txbJmbg.TabIndex = 2;
            // 
            // txbMestoRodjenja
            // 
            this.txbMestoRodjenja.Location = new System.Drawing.Point(16, 109);
            this.txbMestoRodjenja.Name = "txbMestoRodjenja";
            this.txbMestoRodjenja.Size = new System.Drawing.Size(149, 22);
            this.txbMestoRodjenja.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPrikaziVozacku);
            this.groupBox3.Controls.Add(this.btnPrikaziKazne);
            this.groupBox3.Controls.Add(this.btnPrikaziRegistracije);
            this.groupBox3.Controls.Add(this.btnPrikaziPasos);
            this.groupBox3.Controls.Add(this.btnPrikaziLicnuKartu);
            this.groupBox3.Controls.Add(this.btnObrisi);
            this.groupBox3.Controls.Add(this.btnIzmeni);
            this.groupBox3.Controls.Add(this.btnDodaj);
            this.groupBox3.Controls.Add(this.btnPoMestuRodjenja);
            this.groupBox3.Controls.Add(this.btnPoJMBG);
            this.groupBox3.Controls.Add(this.btnPoImenu);
            this.groupBox3.Controls.Add(this.txbMestoRodjenja);
            this.groupBox3.Controls.Add(this.txbIme);
            this.groupBox3.Controls.Add(this.txbJmbg);
            this.groupBox3.Location = new System.Drawing.Point(12, 395);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1017, 209);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            // 
            // btnPrikaziVozacku
            // 
            this.btnPrikaziVozacku.Location = new System.Drawing.Point(836, 151);
            this.btnPrikaziVozacku.Name = "btnPrikaziVozacku";
            this.btnPrikaziVozacku.Size = new System.Drawing.Size(150, 45);
            this.btnPrikaziVozacku.TabIndex = 10;
            this.btnPrikaziVozacku.Text = "Prikazi vozacku dozvolu";
            this.btnPrikaziVozacku.UseVisualStyleBackColor = true;
            this.btnPrikaziVozacku.Click += new System.EventHandler(this.btnPrikaziVozacku_Click);
            // 
            // btnPrikaziKazne
            // 
            this.btnPrikaziKazne.Location = new System.Drawing.Point(637, 151);
            this.btnPrikaziKazne.Name = "btnPrikaziKazne";
            this.btnPrikaziKazne.Size = new System.Drawing.Size(150, 45);
            this.btnPrikaziKazne.TabIndex = 9;
            this.btnPrikaziKazne.Text = "Prikazi saobracajne kazne";
            this.btnPrikaziKazne.UseVisualStyleBackColor = true;
            this.btnPrikaziKazne.Click += new System.EventHandler(this.btnPrikaziKazne_Click);
            // 
            // btnPrikaziRegistracije
            // 
            this.btnPrikaziRegistracije.Location = new System.Drawing.Point(421, 151);
            this.btnPrikaziRegistracije.Name = "btnPrikaziRegistracije";
            this.btnPrikaziRegistracije.Size = new System.Drawing.Size(150, 45);
            this.btnPrikaziRegistracije.TabIndex = 8;
            this.btnPrikaziRegistracije.Text = "Prikazi registracije vozila";
            this.btnPrikaziRegistracije.UseVisualStyleBackColor = true;
            this.btnPrikaziRegistracije.Click += new System.EventHandler(this.btnPrikaziRegistracije_Click);
            // 
            // btnPrikaziPasos
            // 
            this.btnPrikaziPasos.Location = new System.Drawing.Point(218, 151);
            this.btnPrikaziPasos.Name = "btnPrikaziPasos";
            this.btnPrikaziPasos.Size = new System.Drawing.Size(150, 45);
            this.btnPrikaziPasos.TabIndex = 7;
            this.btnPrikaziPasos.Text = "Prikazi pasos";
            this.btnPrikaziPasos.UseVisualStyleBackColor = true;
            this.btnPrikaziPasos.Click += new System.EventHandler(this.btnPrikaziPasos_Click);
            // 
            // btnPrikaziLicnuKartu
            // 
            this.btnPrikaziLicnuKartu.Location = new System.Drawing.Point(16, 151);
            this.btnPrikaziLicnuKartu.Name = "btnPrikaziLicnuKartu";
            this.btnPrikaziLicnuKartu.Size = new System.Drawing.Size(150, 45);
            this.btnPrikaziLicnuKartu.TabIndex = 6;
            this.btnPrikaziLicnuKartu.Text = "Prikazi licnu kartu";
            this.btnPrikaziLicnuKartu.UseVisualStyleBackColor = true;
            this.btnPrikaziLicnuKartu.Click += new System.EventHandler(this.btnPrikaziLicnuKartu_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(637, 104);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(349, 32);
            this.btnObrisi.TabIndex = 13;
            this.btnObrisi.Text = "Obrisi korisnika";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(637, 59);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(349, 32);
            this.btnIzmeni.TabIndex = 12;
            this.btnIzmeni.Text = "Izmeni korisnika";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(637, 16);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(349, 32);
            this.btnDodaj.TabIndex = 11;
            this.btnDodaj.Text = "Dodaj korisnika";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnPoMestuRodjenja
            // 
            this.btnPoMestuRodjenja.Location = new System.Drawing.Point(183, 104);
            this.btnPoMestuRodjenja.Name = "btnPoMestuRodjenja";
            this.btnPoMestuRodjenja.Size = new System.Drawing.Size(242, 32);
            this.btnPoMestuRodjenja.TabIndex = 5;
            this.btnPoMestuRodjenja.Text = "Pretrazi korisnike po mestu rodjenja";
            this.btnPoMestuRodjenja.UseVisualStyleBackColor = true;
            this.btnPoMestuRodjenja.Click += new System.EventHandler(this.btnPoMestuRodjenja_Click);
            // 
            // btnPoJMBG
            // 
            this.btnPoJMBG.Location = new System.Drawing.Point(183, 59);
            this.btnPoJMBG.Name = "btnPoJMBG";
            this.btnPoJMBG.Size = new System.Drawing.Size(242, 32);
            this.btnPoJMBG.TabIndex = 3;
            this.btnPoJMBG.Text = "Pretrazi korisnike po JMBG-u";
            this.btnPoJMBG.UseVisualStyleBackColor = true;
            this.btnPoJMBG.Click += new System.EventHandler(this.btnPoJMBG_Click);
            // 
            // btnPoImenu
            // 
            this.btnPoImenu.Location = new System.Drawing.Point(183, 16);
            this.btnPoImenu.Name = "btnPoImenu";
            this.btnPoImenu.Size = new System.Drawing.Size(242, 32);
            this.btnPoImenu.TabIndex = 1;
            this.btnPoImenu.Text = "Pretrazi korisnike po imenu";
            this.btnPoImenu.UseVisualStyleBackColor = true;
            this.btnPoImenu.Click += new System.EventHandler(this.btnPoImenu_Click);
            // 
            // FormKorisnici
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 613);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(1060, 660);
            this.MinimumSize = new System.Drawing.Size(1060, 660);
            this.Name = "FormKorisnici";
            this.Text = "KORISNICI";
            this.Load += new System.EventHandler(this.FormKorisnici_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSaLK;
        private System.Windows.Forms.TextBox txbIme;
        private System.Windows.Forms.Button btnSaP;
        private System.Windows.Forms.Button btnSaRV;
        private System.Windows.Forms.Button btnSaSK;
        private System.Windows.Forms.Button btnSaVD;
        private System.Windows.Forms.Button btnBezVD;
        private System.Windows.Forms.Button btnBezSK;
        private System.Windows.Forms.Button btnBezRV;
        private System.Windows.Forms.Button btnBezP;
        private System.Windows.Forms.Button btnBezLK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView korisnici;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TextBox txbJmbg;
        private System.Windows.Forms.TextBox txbMestoRodjenja;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPoMestuRodjenja;
        private System.Windows.Forms.Button btnPoJMBG;
        private System.Windows.Forms.Button btnPoImenu;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btnPrikaziVozacku;
        private System.Windows.Forms.Button btnPrikaziKazne;
        private System.Windows.Forms.Button btnPrikaziRegistracije;
        private System.Windows.Forms.Button btnPrikaziPasos;
        private System.Windows.Forms.Button btnPrikaziLicnuKartu;
    }
}