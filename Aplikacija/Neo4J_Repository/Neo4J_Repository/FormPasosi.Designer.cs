﻿namespace Neo4J_Repository
{
    partial class FormPasosi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPoPolu = new System.Windows.Forms.Button();
            this.txbPol = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnObrisiPasos = new System.Windows.Forms.Button();
            this.btnDodajPasos = new System.Windows.Forms.Button();
            this.btnPoPrebivalistu = new System.Windows.Forms.Button();
            this.txbPrebivaliste = new System.Windows.Forms.TextBox();
            this.btnPoDrzavljanstvu = new System.Windows.Forms.Button();
            this.txbDrzavljanstvo = new System.Windows.Forms.TextBox();
            this.btnSviPasosi = new System.Windows.Forms.Button();
            this.btnPoMestuIzdavanja = new System.Windows.Forms.Button();
            this.btnPoDatumuVazenja = new System.Windows.Forms.Button();
            this.btnPoBrojuP = new System.Windows.Forms.Button();
            this.txbMestoIzdavanja = new System.Windows.Forms.TextBox();
            this.txbBrojP = new System.Windows.Forms.TextBox();
            this.txbDatumVazenja = new System.Windows.Forms.TextBox();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pasosi = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPoPolu
            // 
            this.btnPoPolu.Location = new System.Drawing.Point(174, 151);
            this.btnPoPolu.Name = "btnPoPolu";
            this.btnPoPolu.Size = new System.Drawing.Size(586, 32);
            this.btnPoPolu.TabIndex = 7;
            this.btnPoPolu.Text = "Pretrazi pasose po polu";
            this.btnPoPolu.UseVisualStyleBackColor = true;
            this.btnPoPolu.Click += new System.EventHandler(this.btnPoPolu_Click);
            // 
            // txbPol
            // 
            this.txbPol.Location = new System.Drawing.Point(7, 156);
            this.txbPol.Name = "txbPol";
            this.txbPol.Size = new System.Drawing.Size(149, 22);
            this.txbPol.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnObrisiPasos);
            this.groupBox1.Controls.Add(this.btnDodajPasos);
            this.groupBox1.Controls.Add(this.btnPoPrebivalistu);
            this.groupBox1.Controls.Add(this.txbPrebivaliste);
            this.groupBox1.Controls.Add(this.btnPoDrzavljanstvu);
            this.groupBox1.Controls.Add(this.txbDrzavljanstvo);
            this.groupBox1.Controls.Add(this.btnPoPolu);
            this.groupBox1.Controls.Add(this.txbPol);
            this.groupBox1.Controls.Add(this.btnSviPasosi);
            this.groupBox1.Controls.Add(this.btnPoMestuIzdavanja);
            this.groupBox1.Controls.Add(this.btnPoDatumuVazenja);
            this.groupBox1.Controls.Add(this.btnPoBrojuP);
            this.groupBox1.Controls.Add(this.txbMestoIzdavanja);
            this.groupBox1.Controls.Add(this.txbBrojP);
            this.groupBox1.Controls.Add(this.txbDatumVazenja);
            this.groupBox1.Location = new System.Drawing.Point(14, 220);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(940, 279);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // btnObrisiPasos
            // 
            this.btnObrisiPasos.Location = new System.Drawing.Point(766, 194);
            this.btnObrisiPasos.Name = "btnObrisiPasos";
            this.btnObrisiPasos.Size = new System.Drawing.Size(167, 75);
            this.btnObrisiPasos.TabIndex = 14;
            this.btnObrisiPasos.Text = "Obrisi pasos";
            this.btnObrisiPasos.UseVisualStyleBackColor = true;
            this.btnObrisiPasos.Click += new System.EventHandler(this.btnObrisiPasos_Click);
            // 
            // btnDodajPasos
            // 
            this.btnDodajPasos.Location = new System.Drawing.Point(766, 110);
            this.btnDodajPasos.Name = "btnDodajPasos";
            this.btnDodajPasos.Size = new System.Drawing.Size(167, 73);
            this.btnDodajPasos.TabIndex = 13;
            this.btnDodajPasos.Text = "Dodaj pasos";
            this.btnDodajPasos.UseVisualStyleBackColor = true;
            this.btnDodajPasos.Click += new System.EventHandler(this.btnDodajPasos_Click);
            // 
            // btnPoPrebivalistu
            // 
            this.btnPoPrebivalistu.Location = new System.Drawing.Point(174, 237);
            this.btnPoPrebivalistu.Name = "btnPoPrebivalistu";
            this.btnPoPrebivalistu.Size = new System.Drawing.Size(586, 32);
            this.btnPoPrebivalistu.TabIndex = 11;
            this.btnPoPrebivalistu.Text = "Pretrazi pasose po prebivalistu";
            this.btnPoPrebivalistu.UseVisualStyleBackColor = true;
            this.btnPoPrebivalistu.Click += new System.EventHandler(this.btnPoPrebivalistu_Click);
            // 
            // txbPrebivaliste
            // 
            this.txbPrebivaliste.Location = new System.Drawing.Point(7, 242);
            this.txbPrebivaliste.Name = "txbPrebivaliste";
            this.txbPrebivaliste.Size = new System.Drawing.Size(149, 22);
            this.txbPrebivaliste.TabIndex = 10;
            // 
            // btnPoDrzavljanstvu
            // 
            this.btnPoDrzavljanstvu.Location = new System.Drawing.Point(174, 194);
            this.btnPoDrzavljanstvu.Name = "btnPoDrzavljanstvu";
            this.btnPoDrzavljanstvu.Size = new System.Drawing.Size(586, 32);
            this.btnPoDrzavljanstvu.TabIndex = 9;
            this.btnPoDrzavljanstvu.Text = "Pretrazi pasose po drzavljanstvu";
            this.btnPoDrzavljanstvu.UseVisualStyleBackColor = true;
            this.btnPoDrzavljanstvu.Click += new System.EventHandler(this.btnPoDrzavljanstvu_Click);
            // 
            // txbDrzavljanstvo
            // 
            this.txbDrzavljanstvo.Location = new System.Drawing.Point(7, 199);
            this.txbDrzavljanstvo.Name = "txbDrzavljanstvo";
            this.txbDrzavljanstvo.Size = new System.Drawing.Size(149, 22);
            this.txbDrzavljanstvo.TabIndex = 8;
            // 
            // btnSviPasosi
            // 
            this.btnSviPasosi.Location = new System.Drawing.Point(766, 22);
            this.btnSviPasosi.Name = "btnSviPasosi";
            this.btnSviPasosi.Size = new System.Drawing.Size(167, 75);
            this.btnSviPasosi.TabIndex = 12;
            this.btnSviPasosi.Text = "Svi pasosi";
            this.btnSviPasosi.UseVisualStyleBackColor = true;
            this.btnSviPasosi.Click += new System.EventHandler(this.btnSviPasosi_Click);
            // 
            // btnPoMestuIzdavanja
            // 
            this.btnPoMestuIzdavanja.Location = new System.Drawing.Point(174, 65);
            this.btnPoMestuIzdavanja.Name = "btnPoMestuIzdavanja";
            this.btnPoMestuIzdavanja.Size = new System.Drawing.Size(586, 32);
            this.btnPoMestuIzdavanja.TabIndex = 3;
            this.btnPoMestuIzdavanja.Text = "Pretrazi pasose po mestu izdavanja";
            this.btnPoMestuIzdavanja.UseVisualStyleBackColor = true;
            this.btnPoMestuIzdavanja.Click += new System.EventHandler(this.btnPoMestuIzdavanja_Click);
            // 
            // btnPoDatumuVazenja
            // 
            this.btnPoDatumuVazenja.Location = new System.Drawing.Point(174, 110);
            this.btnPoDatumuVazenja.Name = "btnPoDatumuVazenja";
            this.btnPoDatumuVazenja.Size = new System.Drawing.Size(586, 32);
            this.btnPoDatumuVazenja.TabIndex = 5;
            this.btnPoDatumuVazenja.Text = "Pretrazi pasose po datumu vazenja";
            this.btnPoDatumuVazenja.UseVisualStyleBackColor = true;
            this.btnPoDatumuVazenja.Click += new System.EventHandler(this.btnPoDatumuVazenja_Click);
            // 
            // btnPoBrojuP
            // 
            this.btnPoBrojuP.Location = new System.Drawing.Point(174, 22);
            this.btnPoBrojuP.Name = "btnPoBrojuP";
            this.btnPoBrojuP.Size = new System.Drawing.Size(586, 32);
            this.btnPoBrojuP.TabIndex = 1;
            this.btnPoBrojuP.Text = "Pretrazi pasose po broju pasosa";
            this.btnPoBrojuP.UseVisualStyleBackColor = true;
            this.btnPoBrojuP.Click += new System.EventHandler(this.btnPoBrojuP_Click);
            // 
            // txbMestoIzdavanja
            // 
            this.txbMestoIzdavanja.Location = new System.Drawing.Point(7, 70);
            this.txbMestoIzdavanja.Name = "txbMestoIzdavanja";
            this.txbMestoIzdavanja.Size = new System.Drawing.Size(149, 22);
            this.txbMestoIzdavanja.TabIndex = 2;
            // 
            // txbBrojP
            // 
            this.txbBrojP.Location = new System.Drawing.Point(7, 27);
            this.txbBrojP.Name = "txbBrojP";
            this.txbBrojP.Size = new System.Drawing.Size(149, 22);
            this.txbBrojP.TabIndex = 0;
            // 
            // txbDatumVazenja
            // 
            this.txbDatumVazenja.Location = new System.Drawing.Point(7, 115);
            this.txbDatumVazenja.Name = "txbDatumVazenja";
            this.txbDatumVazenja.Size = new System.Drawing.Size(149, 22);
            this.txbDatumVazenja.TabIndex = 4;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Pol";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vazi do";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Izdat od";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 100;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Broj pasosa";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 100;
            // 
            // pasosi
            // 
            this.pasosi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.pasosi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pasosi.FullRowSelect = true;
            this.pasosi.GridLines = true;
            this.pasosi.Location = new System.Drawing.Point(4, 19);
            this.pasosi.Margin = new System.Windows.Forms.Padding(4);
            this.pasosi.Name = "pasosi";
            this.pasosi.Size = new System.Drawing.Size(936, 178);
            this.pasosi.TabIndex = 1;
            this.pasosi.UseCompatibleStateImageBehavior = false;
            this.pasosi.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Vazi od";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Drzavljanstvo";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Prebivaliste";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 100;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pasosi);
            this.groupBox2.Location = new System.Drawing.Point(14, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(944, 201);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista pasosa";
            // 
            // FormPasosi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 513);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MaximumSize = new System.Drawing.Size(990, 560);
            this.MinimumSize = new System.Drawing.Size(990, 560);
            this.Name = "FormPasosi";
            this.Text = "PASOSI";
            this.Load += new System.EventHandler(this.FormPasosi_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPoPolu;
        private System.Windows.Forms.TextBox txbPol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSviPasosi;
        private System.Windows.Forms.Button btnPoMestuIzdavanja;
        private System.Windows.Forms.Button btnPoDatumuVazenja;
        private System.Windows.Forms.Button btnPoBrojuP;
        private System.Windows.Forms.TextBox txbMestoIzdavanja;
        private System.Windows.Forms.TextBox txbBrojP;
        private System.Windows.Forms.TextBox txbDatumVazenja;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView pasosi;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnPoPrebivalistu;
        private System.Windows.Forms.TextBox txbPrebivaliste;
        private System.Windows.Forms.Button btnPoDrzavljanstvu;
        private System.Windows.Forms.TextBox txbDrzavljanstvo;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Button btnDodajPasos;
        private System.Windows.Forms.Button btnObrisiPasos;
    }
}