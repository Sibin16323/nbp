﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormIzmeniKorisnika : Form
    {
        public GraphClient client;
        Korisnik korisnik;
        public FormIzmeniKorisnika()
        {
            InitializeComponent();
        }

        public FormIzmeniKorisnika(string korisnikIme, string korisnikPrezime, string korisnikJmbg, string korisnikDatumRodjenja, string korisnikMestoRodjenja)
        {
            InitializeComponent();
            korisnik = this.kreirajKorisnika(korisnikIme, korisnikPrezime, korisnikJmbg, korisnikDatumRodjenja, korisnikMestoRodjenja);
            popuniPodacima();
        }

        public void popuniPodacima()
        {
            txbUnosIme.Text = korisnik.ime;
            txbUnosPrezime.Text = korisnik.prezime;
            txbUnosJMBG.Text = korisnik.jmbg;
            dtpDatumRodjenja.Text = korisnik.datumRodjenja;
            txbUnosMestoRodjenja.Text = korisnik.mestoRodjenja;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            korisnik.ime = txbUnosIme.Text;
            korisnik.prezime = txbUnosPrezime.Text;
            korisnik.jmbg = txbUnosJMBG.Text;
            korisnik.datumRodjenja = dtpDatumRodjenja.Text;
            korisnik.mestoRodjenja = txbUnosMestoRodjenja.Text;

            if (txbUnosIme.Text == "")
            {
                MessageBox.Show("Niste uneli ime korisnika!");
                return;
            }

            if (txbUnosPrezime.Text == "")
            {
                MessageBox.Show("Niste uneli prezime korisnika!");
                return;
            }

            if (txbUnosJMBG.Text == "")
            {
                MessageBox.Show("Niste uneli JMBG korisnika!");
                return;
            }

            if (txbUnosJMBG.TextLength < 13)
            {
                MessageBox.Show("JMBG mora imati 13 cifara!");
                return;
            }

            if (txbUnosMestoRodjenja.Text == "")
            {
                MessageBox.Show("Niste uneli mesto rodjenja korisnika!");
                return;
            }

            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                korisnik.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                korisnik.id = "";
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ime", txbUnosIme.Text);
            queryDict.Add("prezime", txbUnosPrezime.Text);
            queryDict.Add("jmbg", txbUnosJMBG.Text);
            queryDict.Add("datumRodjenja", dtpDatumRodjenja.Value.ToString());
            queryDict.Add("mestoRodjenja", txbUnosMestoRodjenja.Text);

            // KREIRANJE CVORA KORISNIK
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Korisnik {jmbg:'" + korisnik.jmbg + "'})"
                                                                                                        + " set n.ime = '" + korisnik.ime
                                                                                                        + "', n.prezime = '" + korisnik.prezime
                                                                                                        + "', n.jmbg = '" + korisnik.jmbg
                                                                                                        + "', n.datumRodjenja = '" + korisnik.datumRodjenja
                                                                                                        + "', n.mestoRodjenja = '" + korisnik.mestoRodjenja
                                                                                                        + "' return n",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> korisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (Korisnik k in korisnici)
            {
                MessageBox.Show("Korisnik: " + k.ime + " " + k.prezime + " je uspesno izmenjen!");
            }

            this.Close();
        }

        private Korisnik kreirajKorisnika(string ime, string prezime, string jmbg, string dr, string mr)
        {
            Korisnik korisnik = new Korisnik();

            korisnik.ime = ime;
            korisnik.prezime = prezime;
            korisnik.jmbg = jmbg;
            korisnik.datumRodjenja = dr;
            korisnik.mestoRodjenja = mr;

            return korisnik;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
