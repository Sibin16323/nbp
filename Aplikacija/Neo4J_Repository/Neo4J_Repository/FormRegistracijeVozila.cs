﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormRegistracijeVozila : Form
    {
        public GraphClient client;
        public FormRegistracijeVozila()
        {
            InitializeComponent();
        }

        private void btnPoRegistarskomBroju_Click(object sender, EventArgs e)
        {
            if (txbRegistarskiBroj.Text == "")
            {
                MessageBox.Show("Niste uneli registarski broj (Format: 'AA-000AA')!");
                return;
            }

            string registarskiBroj = ".*" + txbRegistarskiBroj.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("registarskiBroj", registarskiBroj);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RegistracijaVozila) and exists(n.registarskiBroj) and n.registarskiBroj =~ {registarskiBroj} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<RegistracijaVozila> sveRegistracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            this.registracijeVozila.Items.Clear();

            foreach (RegistracijaVozila rv in sveRegistracije)
            {
                ListViewItem item = new ListViewItem(new string[] { rv.registarskiBroj, rv.vaziDo, rv.kategorija });
                this.registracijeVozila.Items.Add(item);
            }

            this.registracijeVozila.Refresh();
        }

        private void btnPoDatumuVazenja_Click(object sender, EventArgs e)
        {
            if (txbDatumVazenja.Text == "")
            {
                MessageBox.Show("Niste uneli datum vazenja registracije vozila!");
                return;
            }

            string datumVazenja = ".*" + txbDatumVazenja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datumVazenja", datumVazenja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RegistracijaVozila) and exists(n.vaziDo) and n.vaziDo =~ {datumVazenja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<RegistracijaVozila> sveRegistracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            this.registracijeVozila.Items.Clear();

            foreach (RegistracijaVozila rv in sveRegistracije)
            {
                ListViewItem item = new ListViewItem(new string[] { rv.registarskiBroj, rv.vaziDo, rv.kategorija });
                this.registracijeVozila.Items.Add(item);
            }

            this.registracijeVozila.Refresh();
        }

        private void btnPoKategoriji_Click(object sender, EventArgs e)
        {
            if (txbKategorija.Text == "")
            {
                MessageBox.Show("Niste uneli kategoriju registracije vozila!");
                return;
            }

            string kategorija = ".*" + txbKategorija.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("kategorija", kategorija);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RegistracijaVozila) and exists(n.kategorija) and n.kategorija =~ {kategorija} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<RegistracijaVozila> sveRegistracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            this.registracijeVozila.Items.Clear();

            foreach (RegistracijaVozila rv in sveRegistracije)
            {
                ListViewItem item = new ListViewItem(new string[] { rv.registarskiBroj, rv.vaziDo, rv.kategorija });
                this.registracijeVozila.Items.Add(item);
            }

            this.registracijeVozila.Refresh();
        }

        private void btnSveRegistracijeVozila_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RegistracijaVozila) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RegistracijaVozila> sveRegistracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            this.registracijeVozila.Items.Clear();

            foreach (RegistracijaVozila rv in sveRegistracije)
            {
                ListViewItem item = new ListViewItem(new string[] { rv.registarskiBroj, rv.vaziDo, rv.kategorija });
                this.registracijeVozila.Items.Add(item);
            }

            this.registracijeVozila.Refresh();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormDodajRegistracijuVozila DodajRegistracijuVozilaForm = new FormDodajRegistracijuVozila();
            DodajRegistracijuVozilaForm.client = client;
            DodajRegistracijuVozilaForm.ShowDialog();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (registracijeVozila.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite registraciju vozila koju zelite da obrisete!");
                return;
            }

            string registarskiBroj = registracijeVozila.SelectedItems[0].SubItems[0].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("registarskiBroj", registarskiBroj);

            // PRVO BRISEMO RELACIJE
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (a)-[r]-(b:RegistracijaVozila{registarskiBroj:{registarskiBroj}}) delete r",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            // DRUGO BRISEMO REGISTRACIJU VOZILA
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RegistracijaVozila{registarskiBroj:{registarskiBroj}}) delete n",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            MessageBox.Show("Registracija vozila je uspesno obrisana sa svim njenim relacijama!");
        }

        private void FormRegistracijeVozila_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RegistracijaVozila) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RegistracijaVozila> sveRegistracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            this.registracijeVozila.Items.Clear();

            foreach (RegistracijaVozila rv in sveRegistracije)
            {
                ListViewItem item = new ListViewItem(new string[] { rv.registarskiBroj, rv.vaziDo, rv.kategorija });
                this.registracijeVozila.Items.Add(item);
            }

            this.registracijeVozila.Refresh();
        }
    }
}
