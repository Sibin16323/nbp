﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajRadnika : Form
    {
        public GraphClient client;
        public FormDodajRadnika()
        {
            InitializeComponent();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (txbUnosIme.Text == "")
            {
                MessageBox.Show("Niste uneli ime radnika!");
                return;
            }

            if (txbUnosPrezime.Text == "")
            {
                MessageBox.Show("Niste uneli prezime radnika!");
                return;
            }

            if (txbUnosJMBG.Text == "")
            {
                MessageBox.Show("Niste uneli JMBG radnika!");
                return;
            }

            if (txbUnosJMBG.TextLength < 13)
            {
                MessageBox.Show("JMBG mora imati 13 cifara!");
                return;
            }

            string smenaText = "";
            if (rdbPrva.Checked == true) { smenaText = "Prva"; }
            else if (rdbDruga.Checked == true) { smenaText = "Druga"; }
            else
            {
                MessageBox.Show("Morate odabrati smenu radnika!");
                return;
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ime", txbUnosIme.Text);
            queryDict.Add("prezime", txbUnosPrezime.Text);
            queryDict.Add("jmbg", txbUnosJMBG.Text);
            queryDict.Add("datumRodjenja", dtpDatumRodjenja.Value.ToString());
            queryDict.Add("smena", smenaText);

            if (rdbLK.Checked == true)
            {
                RadnikLicnaKarta rad = this.kreirajRadnikaLK();
                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    rad.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    rad.id = "";
                }

                // KREIRANJE CVORA RADNIK LK
                var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:RadnikLicnaKarta {ime:'" + rad.ime
                                                                + "', prezime:'" + rad.prezime
                                                                + "', jmbg:'" + rad.jmbg
                                                                + "', datumRodjenja:'" + rad.datumRodjenja
                                                                + "', smena:'" + rad.smena
                                                                + "'}) return n",
                                                                queryDict, CypherResultMode.Set);

                List<RadnikLicnaKarta> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikLicnaKarta r in radnici)
                {
                    MessageBox.Show("Radnik: " + r.ime + " " + r.prezime + " je uspesno kreiran za salter LICNE KARTE!");
                }
            }
            else if (rdbP.Checked == true)
            {
                RadnikPasos rad = this.kreirajRadnikaP();
                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    rad.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    rad.id = "";
                }

                // KREIRANJE CVORA RADNIK P
                var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:RadnikPasos {ime:'" + rad.ime
                                                                + "', prezime:'" + rad.prezime
                                                                + "', jmbg:'" + rad.jmbg
                                                                + "', datumRodjenja:'" + rad.datumRodjenja
                                                                + "', smena:'" + rad.smena
                                                                + "'}) return n",
                                                                queryDict, CypherResultMode.Set);

                List<RadnikPasos> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikPasos r in radnici)
                {
                    MessageBox.Show("Radnik: " + r.ime + " " + r.prezime + " je uspesno kreiran za salter PASOSI!");
                }
            }
            else if (rdbRV.Checked == true)
            {
                RadnikRegistracijaVozila rad = this.kreirajRadnikaRV();
                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    rad.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    rad.id = "";
                }

                // KREIRANJE CVORA RADNIK RV
                var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:RadnikRegistracijaVozila {ime:'" + rad.ime
                                                                + "', prezime:'" + rad.prezime
                                                                + "', jmbg:'" + rad.jmbg
                                                                + "', datumRodjenja:'" + rad.datumRodjenja
                                                                + "', smena:'" + rad.smena
                                                                + "'}) return n",
                                                                queryDict, CypherResultMode.Set);

                List<RadnikRegistracijaVozila> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikRegistracijaVozila r in radnici)
                {
                    MessageBox.Show("Radnik: " + r.ime + " " + r.prezime + " je uspesno kreiran za salter REGISTRACIJE VOZILA!");
                }
            }
            else if (rdbSK.Checked == true)
            {
                RadnikSaobracajniPolicajac rad = this.kreirajRadnikaSK();
                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    rad.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    rad.id = "";
                }

                // KREIRANJE CVORA RADNIK SK
                var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:RadnikSaobracajniPolicajac {ime:'" + rad.ime
                                                                + "', prezime:'" + rad.prezime
                                                                + "', jmbg:'" + rad.jmbg
                                                                + "', datumRodjenja:'" + rad.datumRodjenja
                                                                + "', smena:'" + rad.smena
                                                                + "'}) return n",
                                                                queryDict, CypherResultMode.Set);

                List<RadnikSaobracajniPolicajac> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikSaobracajniPolicajac r in radnici)
                {
                    MessageBox.Show("Radnik: " + r.ime + " " + r.prezime + " je uspesno kreiran za salter SAOBRACAJNE KAZNE!");
                }
            }
            else if (rdbVD.Checked == true)
            {
                RadnikVozackaDozvola rad = this.kreirajRadnikaVD();
                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    rad.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    rad.id = "";
                }

                // KREIRANJE CVORA RADNIK VD
                var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:RadnikVozackaDozvola {ime:'" + rad.ime
                                                                + "', prezime:'" + rad.prezime
                                                                + "', jmbg:'" + rad.jmbg
                                                                + "', datumRodjenja:'" + rad.datumRodjenja
                                                                + "', smena:'" + rad.smena
                                                                + "'}) return n",
                                                                queryDict, CypherResultMode.Set);

                List<RadnikVozackaDozvola> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikVozackaDozvola r in radnici)
                {
                    MessageBox.Show("Radnik: " + r.ime + " " + r.prezime + " je uspesno kreiran za salter VOZACKE DOZVOLE!");
                }
            }
            else
            {
                MessageBox.Show("Morate odabrati salter za koji zelite da dodate radnika!");
                return;
            }

            this.Close();
        }

        private RadnikLicnaKarta kreirajRadnikaLK()
        {
            RadnikLicnaKarta radnik = new RadnikLicnaKarta();

            radnik.ime = txbUnosIme.Text;
            radnik.prezime = txbUnosPrezime.Text;
            radnik.jmbg = txbUnosJMBG.Text;
            radnik.datumRodjenja = dtpDatumRodjenja.Value.Date.ToString("dd.MM.yyyy.");
            if (rdbPrva.Checked == true) { radnik.smena = "Prva"; }
            else if (rdbDruga.Checked == true) { radnik.smena = "Druga"; }

            return radnik;
        }

        private RadnikPasos kreirajRadnikaP()
        {
            RadnikPasos radnik = new RadnikPasos();

            radnik.ime = txbUnosIme.Text;
            radnik.prezime = txbUnosPrezime.Text;
            radnik.jmbg = txbUnosJMBG.Text;
            radnik.datumRodjenja = dtpDatumRodjenja.Value.Date.ToString("dd.MM.yyyy.");
            if (rdbPrva.Checked == true) { radnik.smena = "Prva"; }
            else if (rdbDruga.Checked == true) { radnik.smena = "Druga"; }

            return radnik;
        }

        private RadnikRegistracijaVozila kreirajRadnikaRV()
        {
            RadnikRegistracijaVozila radnik = new RadnikRegistracijaVozila();

            radnik.ime = txbUnosIme.Text;
            radnik.prezime = txbUnosPrezime.Text;
            radnik.jmbg = txbUnosJMBG.Text;
            radnik.datumRodjenja = dtpDatumRodjenja.Value.Date.ToString("dd.MM.yyyy.");
            if (rdbPrva.Checked == true) { radnik.smena = "Prva"; }
            else if (rdbDruga.Checked == true) { radnik.smena = "Druga"; }

            return radnik;
        }

        private RadnikSaobracajniPolicajac kreirajRadnikaSK()
        {
            RadnikSaobracajniPolicajac radnik = new RadnikSaobracajniPolicajac();

            radnik.ime = txbUnosIme.Text;
            radnik.prezime = txbUnosPrezime.Text;
            radnik.jmbg = txbUnosJMBG.Text;
            radnik.datumRodjenja = dtpDatumRodjenja.Value.Date.ToString("dd.MM.yyyy.");
            if (rdbPrva.Checked == true) { radnik.smena = "Prva"; }
            else if (rdbDruga.Checked == true) { radnik.smena = "Druga"; }

            return radnik;
        }

        private RadnikVozackaDozvola kreirajRadnikaVD()
        {
            RadnikVozackaDozvola radnik = new RadnikVozackaDozvola();

            radnik.ime = txbUnosIme.Text;
            radnik.prezime = txbUnosPrezime.Text;
            radnik.jmbg = txbUnosJMBG.Text;
            radnik.datumRodjenja = dtpDatumRodjenja.Value.Date.ToString("dd.MM.yyyy.");
            if (rdbPrva.Checked == true) { radnik.smena = "Prva"; }
            else if (rdbDruga.Checked == true) { radnik.smena = "Druga"; }

            return radnik;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
