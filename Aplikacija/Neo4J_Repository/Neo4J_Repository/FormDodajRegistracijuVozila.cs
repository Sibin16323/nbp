﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajRegistracijuVozila : Form
    {
        public GraphClient client;
        List<Korisnik> sviKorisnici;
        List<RadnikRegistracijaVozila> sviRadnici;
        RegistracijaVozila dokument;
        public FormDodajRegistracijuVozila()
        {
            InitializeComponent();
        }

        private void FormDodajRegistracijuVozila_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_RV]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            foreach (Korisnik k in sviKorisnici)
            {
                this.cbxKorisnici.Items.Add(k.ime + " " + k.prezime + " JBMG: " + k.jmbg);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikRegistracijaVozila) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query2).ToList();

            foreach (RadnikRegistracijaVozila r in sviRadnici)
            {
                this.cbxRadnici.Items.Add(r.ime + " " + r.prezime);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (cbxRadnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati radnika koji ce izdati registraciju vozila!");
                return;
            }

            if (cbxKorisnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati korisnika kome ce biti izdata registracija vozila!");
                return;
            }

            if (txbUnosBroj.Text == "")
            {
                MessageBox.Show("Niste uneli registracioni broj (Primer: 'BO-000AA')!");
                return;
            }

            if (txbUnosKategorije.Text == "")
            {
                MessageBox.Show("Niste uneli kategoriju (Primer: 'B')!");
                return;
            }

            RegistracijaVozila registracija = this.kreirajRegistraciju();
            IzdaoRV izdao = this.izdaoRegistraciju();
            PosedujeRV poseduje = this.posedujeRegistraciju();
            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                registracija.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                registracija.id = "";
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("registarskiBroj", txbUnosBroj.Text);
            queryDict.Add("vaziDo", dtpDatumDo.Value.ToString());
            queryDict.Add("kategorija", txbUnosKategorije.Text);

            // SELEKTOVAN KORISNIK
            Korisnik kor = sviKorisnici[cbxKorisnici.SelectedIndex];

            // SELEKTOVAN RADNIK
            RadnikRegistracijaVozila rad = sviRadnici[cbxRadnici.SelectedIndex];

            // KREIRANJE CVORA REGISTRACIJAVOZILA
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:RegistracijaVozila {registarskiBroj:'" + registracija.registarskiBroj
                                                            + "', vaziDo:'" + registracija.vaziDo
                                                            + "', kategorija:'" + registracija.kategorija
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<RegistracijaVozila> registracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            // KREIRANJE RELACIJE KORISNIK -> POSEDUJE
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Korisnik {jmbg:'" + kor.jmbg + "'}), (b:RegistracijaVozila {registarskiBroj:'" + registracija.registarskiBroj + "'}) MERGE (a)-[r:POSEDUJE_RV]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<PosedujeRV> relacije = ((IRawGraphClient)client).ExecuteGetCypherResults<PosedujeRV>(query2).ToList();

            // KREIRANJE RELACIJE RADNIK -> IZDAO
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:RadnikRegistracijaVozila {jmbg:'" + rad.jmbg + "'}), (b:RegistracijaVozila {registarskiBroj:'" + registracija.registarskiBroj + "'}) MERGE (a)-[r:IZDAO_RV]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<IzdaoRV> relacije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<IzdaoRV>(query3).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (RegistracijaVozila r in registracije)
            {
                MessageBox.Show("Registracija sa tablicama: " + r.registarskiBroj + " je uspesno kreirana za korisnika " + kor.ime + " " + kor.prezime + "!");
            }

            this.Close();
        }

        private RegistracijaVozila kreirajRegistraciju()
        {
            dokument = new RegistracijaVozila();

            dokument.registarskiBroj = txbUnosBroj.Text;
            dokument.vaziDo = dtpDatumDo.Value.Date.ToString("dd.MM.yyyy.");
            dokument.kategorija = txbUnosKategorije.Text;

            return dokument;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private IzdaoRV izdaoRegistraciju()
        {
            IzdaoRV r = new IzdaoRV();

            RadnikRegistracijaVozila radnik2 = sviRadnici[cbxRadnici.SelectedIndex];

            r.radnik = radnik2;
            r.dokument = this.dokument;

            return r;
        }

        private PosedujeRV posedujeRegistraciju()
        {
            PosedujeRV r = new PosedujeRV();

            Korisnik korisnik2 = sviKorisnici[cbxKorisnici.SelectedIndex];

            r.korisnik = korisnik2;
            r.registracijaVozila = this.dokument;

            return r;
        }
    }
}
