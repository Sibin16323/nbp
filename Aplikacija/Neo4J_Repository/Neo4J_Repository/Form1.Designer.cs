﻿namespace Neo4J_Repository
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnRadnici = new System.Windows.Forms.Button();
            this.btnKorisnici = new System.Windows.Forms.Button();
            this.btnLicneKarte = new System.Windows.Forms.Button();
            this.btnPasosi = new System.Windows.Forms.Button();
            this.btnRegistracijaVozila = new System.Windows.Forms.Button();
            this.btnSaobracajneKazne = new System.Windows.Forms.Button();
            this.btnVozackeDozvole = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRadnici
            // 
            this.btnRadnici.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnRadnici.Location = new System.Drawing.Point(23, 167);
            this.btnRadnici.Name = "btnRadnici";
            this.btnRadnici.Size = new System.Drawing.Size(205, 99);
            this.btnRadnici.TabIndex = 16;
            this.btnRadnici.Text = "RADNICI";
            this.btnRadnici.UseVisualStyleBackColor = true;
            this.btnRadnici.Click += new System.EventHandler(this.btnRadnici_Click);
            // 
            // btnKorisnici
            // 
            this.btnKorisnici.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnKorisnici.Location = new System.Drawing.Point(518, 167);
            this.btnKorisnici.Name = "btnKorisnici";
            this.btnKorisnici.Size = new System.Drawing.Size(205, 99);
            this.btnKorisnici.TabIndex = 17;
            this.btnKorisnici.Text = "KORISNICI";
            this.btnKorisnici.UseVisualStyleBackColor = true;
            this.btnKorisnici.Click += new System.EventHandler(this.btnKorisnici_Click);
            // 
            // btnLicneKarte
            // 
            this.btnLicneKarte.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnLicneKarte.Location = new System.Drawing.Point(42, 395);
            this.btnLicneKarte.Name = "btnLicneKarte";
            this.btnLicneKarte.Size = new System.Drawing.Size(205, 61);
            this.btnLicneKarte.TabIndex = 18;
            this.btnLicneKarte.Text = "LICNE KARTE";
            this.btnLicneKarte.UseVisualStyleBackColor = true;
            this.btnLicneKarte.Click += new System.EventHandler(this.btnLicneKarte_Click);
            // 
            // btnPasosi
            // 
            this.btnPasosi.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnPasosi.Location = new System.Drawing.Point(273, 395);
            this.btnPasosi.Name = "btnPasosi";
            this.btnPasosi.Size = new System.Drawing.Size(205, 61);
            this.btnPasosi.TabIndex = 19;
            this.btnPasosi.Text = "PASOSI";
            this.btnPasosi.UseVisualStyleBackColor = true;
            this.btnPasosi.Click += new System.EventHandler(this.btnPasosi_Click);
            // 
            // btnRegistracijaVozila
            // 
            this.btnRegistracijaVozila.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnRegistracijaVozila.Location = new System.Drawing.Point(504, 395);
            this.btnRegistracijaVozila.Name = "btnRegistracijaVozila";
            this.btnRegistracijaVozila.Size = new System.Drawing.Size(205, 61);
            this.btnRegistracijaVozila.TabIndex = 20;
            this.btnRegistracijaVozila.Text = "REGISTRACIJA VOZILA";
            this.btnRegistracijaVozila.UseVisualStyleBackColor = true;
            this.btnRegistracijaVozila.Click += new System.EventHandler(this.btnRegistracijaVozila_Click);
            // 
            // btnSaobracajneKazne
            // 
            this.btnSaobracajneKazne.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnSaobracajneKazne.Location = new System.Drawing.Point(156, 482);
            this.btnSaobracajneKazne.Name = "btnSaobracajneKazne";
            this.btnSaobracajneKazne.Size = new System.Drawing.Size(205, 61);
            this.btnSaobracajneKazne.TabIndex = 21;
            this.btnSaobracajneKazne.Text = "SAOBRACAJNE KAZNE";
            this.btnSaobracajneKazne.UseVisualStyleBackColor = true;
            this.btnSaobracajneKazne.Click += new System.EventHandler(this.btnSaobracajneKazne_Click);
            // 
            // btnVozackeDozvole
            // 
            this.btnVozackeDozvole.Font = new System.Drawing.Font("Modern No. 20", 12.8F, System.Drawing.FontStyle.Bold);
            this.btnVozackeDozvole.Location = new System.Drawing.Point(391, 482);
            this.btnVozackeDozvole.Name = "btnVozackeDozvole";
            this.btnVozackeDozvole.Size = new System.Drawing.Size(205, 61);
            this.btnVozackeDozvole.TabIndex = 22;
            this.btnVozackeDozvole.Text = "VOZACKE DOZVOLE";
            this.btnVozackeDozvole.UseVisualStyleBackColor = true;
            this.btnVozackeDozvole.Click += new System.EventHandler(this.btnVozackeDozvole_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(257, 80);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(236, 279);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(189, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(374, 35);
            this.label1.TabIndex = 24;
            this.label1.Text = "POLICIJSKA UPRAVA";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 563);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnVozackeDozvole);
            this.Controls.Add(this.btnSaobracajneKazne);
            this.Controls.Add(this.btnRegistracijaVozila);
            this.Controls.Add(this.btnPasosi);
            this.Controls.Add(this.btnLicneKarte);
            this.Controls.Add(this.btnKorisnici);
            this.Controls.Add(this.btnRadnici);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(770, 610);
            this.MinimumSize = new System.Drawing.Size(770, 610);
            this.Name = "Form1";
            this.Text = "POLICIJSKA UPRAVA";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnRadnici;
        private System.Windows.Forms.Button btnKorisnici;
        private System.Windows.Forms.Button btnLicneKarte;
        private System.Windows.Forms.Button btnPasosi;
        private System.Windows.Forms.Button btnRegistracijaVozila;
        private System.Windows.Forms.Button btnSaobracajneKazne;
        private System.Windows.Forms.Button btnVozackeDozvole;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}

