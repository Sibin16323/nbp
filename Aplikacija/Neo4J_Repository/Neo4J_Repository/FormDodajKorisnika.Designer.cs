﻿namespace Neo4J_Repository
{
    partial class FormDodajKorisnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.txbUnosMestoRodjenja = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.txbUnosJMBG = new System.Windows.Forms.TextBox();
            this.dtpDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txbUnosPrezime = new System.Windows.Forms.TextBox();
            this.txbUnosIme = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 17);
            this.label8.TabIndex = 27;
            this.label8.Text = "Unesite mesto rodjenja:";
            // 
            // txbUnosMestoRodjenja
            // 
            this.txbUnosMestoRodjenja.Location = new System.Drawing.Point(188, 205);
            this.txbUnosMestoRodjenja.MaxLength = 100;
            this.txbUnosMestoRodjenja.Name = "txbUnosMestoRodjenja";
            this.txbUnosMestoRodjenja.Size = new System.Drawing.Size(100, 22);
            this.txbUnosMestoRodjenja.TabIndex = 4;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(372, 33);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(109, 194);
            this.btnDodaj.TabIndex = 5;
            this.btnDodaj.Text = "Dodaj korisnika";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // txbUnosJMBG
            // 
            this.txbUnosJMBG.Location = new System.Drawing.Point(188, 121);
            this.txbUnosJMBG.MaxLength = 13;
            this.txbUnosJMBG.Name = "txbUnosJMBG";
            this.txbUnosJMBG.Size = new System.Drawing.Size(100, 22);
            this.txbUnosJMBG.TabIndex = 2;
            // 
            // dtpDatumRodjenja
            // 
            this.dtpDatumRodjenja.CustomFormat = "dd.MM.yyyy.";
            this.dtpDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumRodjenja.Location = new System.Drawing.Point(188, 163);
            this.dtpDatumRodjenja.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDatumRodjenja.Name = "dtpDatumRodjenja";
            this.dtpDatumRodjenja.Size = new System.Drawing.Size(160, 22);
            this.dtpDatumRodjenja.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Unesite JMBG:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Unesite datum rodjenja:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txbUnosPrezime);
            this.groupBox1.Controls.Add(this.txbUnosIme);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txbUnosMestoRodjenja);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.txbUnosJMBG);
            this.groupBox1.Controls.Add(this.dtpDatumRodjenja);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(497, 244);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodavanje korisnika";
            // 
            // txbUnosPrezime
            // 
            this.txbUnosPrezime.Location = new System.Drawing.Point(188, 76);
            this.txbUnosPrezime.MaxLength = 100;
            this.txbUnosPrezime.Name = "txbUnosPrezime";
            this.txbUnosPrezime.Size = new System.Drawing.Size(100, 22);
            this.txbUnosPrezime.TabIndex = 1;
            // 
            // txbUnosIme
            // 
            this.txbUnosIme.Location = new System.Drawing.Point(188, 33);
            this.txbUnosIme.MaxLength = 100;
            this.txbUnosIme.Name = "txbUnosIme";
            this.txbUnosIme.Size = new System.Drawing.Size(100, 22);
            this.txbUnosIme.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 30;
            this.label2.Text = "Unesite prezime:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 29;
            this.label1.Text = "Unesite ime:";
            // 
            // FormDodajKorisnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 263);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(540, 310);
            this.MinimumSize = new System.Drawing.Size(540, 310);
            this.Name = "FormDodajKorisnika";
            this.Text = "DODAVANJE KORISNIKA";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbUnosMestoRodjenja;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.TextBox txbUnosJMBG;
        private System.Windows.Forms.DateTimePicker dtpDatumRodjenja;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbUnosPrezime;
        private System.Windows.Forms.TextBox txbUnosIme;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}