﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormLicneKarte : Form
    {
        public GraphClient client;
        public FormLicneKarte()
        {
            InitializeComponent();
        }

        private void btnPoBrojuLK_Click(object sender, EventArgs e)
        {
            if (txbBrojLK.Text == "")
            {
                MessageBox.Show("Niste uneli broj licne karte!");
                return;
            }

            string brojLicne = ".*" + txbBrojLK.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojLicne", brojLicne);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:LicnaKarta) and exists(n.brojLK) and n.brojLK =~ {brojLicne} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<LicnaKarta> sveLicne = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            this.licneKarte.Items.Clear();

            foreach (LicnaKarta l in sveLicne)
            {
                ListViewItem item = new ListViewItem(new string[] { l.brojLK, l.izdataOd, l.vaziOd, l.vaziDo, l.pol });
                this.licneKarte.Items.Add(item);
            }

            this.licneKarte.Refresh();
        }

        private void btnPoMestuIzdavanja_Click(object sender, EventArgs e)
        {
            if (txbMestoIzdavanja.Text == "")
            {
                MessageBox.Show("Niste uneli mesto izdavanja licne karte!");
                return;
            }

            string mestoIzdavanja = ".*" + txbMestoIzdavanja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("mestoIzdavanja", mestoIzdavanja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:LicnaKarta) and exists(n.izdataOd) and n.izdataOd =~ {mestoIzdavanja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<LicnaKarta> sveLicne = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            this.licneKarte.Items.Clear();

            foreach (LicnaKarta l in sveLicne)
            {
                ListViewItem item = new ListViewItem(new string[] { l.brojLK, l.izdataOd, l.vaziOd, l.vaziDo, l.pol });
                this.licneKarte.Items.Add(item);
            }

            this.licneKarte.Refresh();
        }

        private void btnPoDatumuVazenja_Click(object sender, EventArgs e)
        {
            if (txbDatumVazenja.Text == "")
            {
                MessageBox.Show("Niste uneli datum vazenja licne karte!");
                return;
            }

            string datumVazenja = ".*" + txbDatumVazenja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datumVazenja", datumVazenja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:LicnaKarta) and exists(n.vaziDo) and n.vaziDo =~ {datumVazenja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<LicnaKarta> sveLicne = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            this.licneKarte.Items.Clear();

            foreach (LicnaKarta l in sveLicne)
            {
                ListViewItem item = new ListViewItem(new string[] { l.brojLK, l.izdataOd, l.vaziOd, l.vaziDo, l.pol });
                this.licneKarte.Items.Add(item);
            }

            this.licneKarte.Refresh();
        }

        private void btnPoPolu_Click(object sender, EventArgs e)
        {
            if (txbPol.Text == "")
            {
                MessageBox.Show("Niste uneli pol!");
                return;
            }
            
            if (txbPol.Text !="M" && txbPol.Text !="Z")
            {
                MessageBox.Show("Validan unos je samo 'M' ili 'Z' (velika slova)!");
                return;
            }

            string pol = ".*" + txbPol.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("pol", pol);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:LicnaKarta) and exists(n.pol) and n.pol =~ {pol} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<LicnaKarta> sveLicne = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            this.licneKarte.Items.Clear();

            foreach (LicnaKarta l in sveLicne)
            {
                ListViewItem item = new ListViewItem(new string[] { l.brojLK, l.izdataOd, l.vaziOd, l.vaziDo, l.pol });
                this.licneKarte.Items.Add(item);
            }

            this.licneKarte.Refresh();
        }

        private void btnSveLicneKarte_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:LicnaKarta) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<LicnaKarta> sveLicne = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            this.licneKarte.Items.Clear();

            foreach (LicnaKarta l in sveLicne)
            {
                ListViewItem item = new ListViewItem(new string[] { l.brojLK, l.izdataOd, l.vaziOd, l.vaziDo, l.pol });
                this.licneKarte.Items.Add(item);
            }

            this.licneKarte.Refresh();
        }

        private void btnDodajLicnuKartu_Click(object sender, EventArgs e)
        {
            FormDodajLicnuKartu DodajLicnukartuForm = new FormDodajLicnuKartu();
            DodajLicnukartuForm.client = client;
            DodajLicnukartuForm.ShowDialog();
        }

        private void btnObrisiLicnuKartu_Click(object sender, EventArgs e)
        {
            if (licneKarte.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite licnu kartu koju zelite da obrisete!");
                return;
            }

            string brojLicneKarte = licneKarte.SelectedItems[0].SubItems[0].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojLicneKarte", brojLicneKarte);

            // PRVO BRISEMO RELACIJE
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (a)-[r]-(b:LicnaKarta{brojLK:{brojLicneKarte}}) delete r",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            // DRUGO BRISEMO LICNU KARTU
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:LicnaKarta{brojLK:{brojLicneKarte}}) delete n",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            MessageBox.Show("Licna karta je uspesno obrisana sa svim njenim relacijama!");
        }

        private void FormLicneKarte_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:LicnaKarta) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<LicnaKarta> sveLicne = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            this.licneKarte.Items.Clear();

            foreach (LicnaKarta l in sveLicne)
            {
                ListViewItem item = new ListViewItem(new string[] { l.brojLK, l.izdataOd, l.vaziOd, l.vaziDo, l.pol });
                this.licneKarte.Items.Add(item);
            }

            this.licneKarte.Refresh();
        }
    }
}
