﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormIzmeniRadnika : Form
    {
        public GraphClient client;
        RadnikLicnaKarta radnikLK;
        RadnikPasos radnikP;
        RadnikRegistracijaVozila radnikRV;
        RadnikSaobracajniPolicajac radnikSK;
        RadnikVozackaDozvola radnikVD;
        int tip;
        public FormIzmeniRadnika()
        {
            InitializeComponent();
        }

        public FormIzmeniRadnika(string radnikIme, string radnikPrezime, string radnikJmbg, string radnikDatumRodjenja, string radnikSmena, int radnikTip)
        {
            InitializeComponent();
            if(radnikTip == 1)
            {
                radnikLK = this.kreirajRadnikaLK(radnikIme, radnikPrezime, radnikJmbg, radnikDatumRodjenja, radnikSmena);
                popuniPodacimaLK();
                tip = radnikTip;
            }
            else if (radnikTip == 2)
            {
                radnikP = this.kreirajRadnikaP(radnikIme, radnikPrezime, radnikJmbg, radnikDatumRodjenja, radnikSmena);
                popuniPodacimaP();
                tip = radnikTip;
            }
            else if (radnikTip == 3)
            {
                radnikRV = this.kreirajRadnikaRV(radnikIme, radnikPrezime, radnikJmbg, radnikDatumRodjenja, radnikSmena);
                popuniPodacimaRV();
                tip = radnikTip;
            }
            else if (radnikTip == 4)
            {
                radnikSK = this.kreirajRadnikaSK(radnikIme, radnikPrezime, radnikJmbg, radnikDatumRodjenja, radnikSmena);
                popuniPodacimaSK();
                tip = radnikTip;
            }
            else if (radnikTip == 5)
            {
                radnikVD = this.kreirajRadnikaVD(radnikIme, radnikPrezime, radnikJmbg, radnikDatumRodjenja, radnikSmena);
                popuniPodacimaVD();
                tip = radnikTip;
            }
        }

        private RadnikLicnaKarta kreirajRadnikaLK(string radnikIme, string radnikPrezime, string radnikJmbg, string radnikDatumRodjenja, string radnikSmena)
        {
            RadnikLicnaKarta radnik = new RadnikLicnaKarta();

            radnik.ime = radnikIme;
            radnik.prezime = radnikPrezime;
            radnik.jmbg = radnikJmbg;
            radnik.datumRodjenja = radnikDatumRodjenja;
            radnik.smena = radnikSmena;

            return radnik;
        }

        private RadnikPasos kreirajRadnikaP(string radnikIme, string radnikPrezime, string radnikJmbg, string radnikDatumRodjenja, string radnikSmena)
        {
            RadnikPasos radnik = new RadnikPasos();

            radnik.ime = radnikIme;
            radnik.prezime = radnikPrezime;
            radnik.jmbg = radnikJmbg;
            radnik.datumRodjenja = radnikDatumRodjenja;
            radnik.smena = radnikSmena;

            return radnik;
        }

        private RadnikRegistracijaVozila kreirajRadnikaRV(string radnikIme, string radnikPrezime, string radnikJmbg, string radnikDatumRodjenja, string radnikSmena)
        {
            RadnikRegistracijaVozila radnik = new RadnikRegistracijaVozila();

            radnik.ime = radnikIme;
            radnik.prezime = radnikPrezime;
            radnik.jmbg = radnikJmbg;
            radnik.datumRodjenja = radnikDatumRodjenja;
            radnik.smena = radnikSmena;

            return radnik;
        }

        private RadnikSaobracajniPolicajac kreirajRadnikaSK(string radnikIme, string radnikPrezime, string radnikJmbg, string radnikDatumRodjenja, string radnikSmena)
        {
            RadnikSaobracajniPolicajac radnik = new RadnikSaobracajniPolicajac();

            radnik.ime = radnikIme;
            radnik.prezime = radnikPrezime;
            radnik.jmbg = radnikJmbg;
            radnik.datumRodjenja = radnikDatumRodjenja;
            radnik.smena = radnikSmena;

            return radnik;
        }

        private RadnikVozackaDozvola kreirajRadnikaVD(string radnikIme, string radnikPrezime, string radnikJmbg, string radnikDatumRodjenja, string radnikSmena)
        {
            RadnikVozackaDozvola radnik = new RadnikVozackaDozvola();

            radnik.ime = radnikIme;
            radnik.prezime = radnikPrezime;
            radnik.jmbg = radnikJmbg;
            radnik.datumRodjenja = radnikDatumRodjenja;
            radnik.smena = radnikSmena;

            return radnik;
        }

        public void popuniPodacimaLK()
        {
            txbUnosIme.Text = radnikLK.ime;
            txbUnosPrezime.Text = radnikLK.prezime;
            txbUnosJMBG.Text = radnikLK.jmbg;
            dtpDatumRodjenja.Text = radnikLK.datumRodjenja;
            if(radnikLK.smena == "Prva") { rdbPrva.Checked = true; rdbDruga.Checked = false; }
            else if (radnikLK.smena == "Druga") { rdbPrva.Checked = false; rdbDruga.Checked = true; }
        }

        public void popuniPodacimaP()
        {
            txbUnosIme.Text = radnikP.ime;
            txbUnosPrezime.Text = radnikP.prezime;
            txbUnosJMBG.Text = radnikP.jmbg;
            dtpDatumRodjenja.Text = radnikP.datumRodjenja;
            if (radnikP.smena == "Prva") { rdbPrva.Checked = true; rdbDruga.Checked = false; }
            else if (radnikP.smena == "Druga") { rdbPrva.Checked = false; rdbDruga.Checked = true; }
        }

        public void popuniPodacimaRV()
        {
            txbUnosIme.Text = radnikRV.ime;
            txbUnosPrezime.Text = radnikRV.prezime;
            txbUnosJMBG.Text = radnikRV.jmbg;
            dtpDatumRodjenja.Text = radnikRV.datumRodjenja;
            if (radnikRV.smena == "Prva") { rdbPrva.Checked = true; rdbDruga.Checked = false; }
            else if (radnikRV.smena == "Druga") { rdbPrva.Checked = false; rdbDruga.Checked = true; }
        }

        public void popuniPodacimaSK()
        {
            txbUnosIme.Text = radnikSK.ime;
            txbUnosPrezime.Text = radnikSK.prezime;
            txbUnosJMBG.Text = radnikSK.jmbg;
            dtpDatumRodjenja.Text = radnikSK.datumRodjenja;
            if (radnikSK.smena == "Prva") { rdbPrva.Checked = true; rdbDruga.Checked = false; }
            else if (radnikSK.smena == "Druga") { rdbPrva.Checked = false; rdbDruga.Checked = true; }
        }

        public void popuniPodacimaVD()
        {
            txbUnosIme.Text = radnikVD.ime;
            txbUnosPrezime.Text = radnikVD.prezime;
            txbUnosJMBG.Text = radnikVD.jmbg;
            dtpDatumRodjenja.Text = radnikVD.datumRodjenja;
            if (radnikVD.smena == "Prva") { rdbPrva.Checked = true; rdbDruga.Checked = false; }
            else if (radnikVD.smena == "Druga") { rdbPrva.Checked = false; rdbDruga.Checked = true; }
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (txbUnosIme.Text == "")
            {
                MessageBox.Show("Niste uneli ime korisnika!");
                return;
            }

            if (txbUnosPrezime.Text == "")
            {
                MessageBox.Show("Niste uneli prezime korisnika!");
                return;
            }

            if (txbUnosJMBG.Text == "")
            {
                MessageBox.Show("Niste uneli JMBG korisnika!");
                return;
            }

            if (txbUnosJMBG.TextLength < 13)
            {
                MessageBox.Show("JMBG mora imati 13 cifara!");
                return;
            }

            if(tip == 1)
            {
                radnikLK.ime = txbUnosIme.Text;
                radnikLK.prezime = txbUnosPrezime.Text;
                radnikLK.jmbg = txbUnosJMBG.Text;
                radnikLK.datumRodjenja = dtpDatumRodjenja.Text;
                if (rdbPrva.Checked == true) { radnikLK.smena = "Prva"; }
                else if (rdbDruga.Checked == true) { radnikLK.smena = "Druga"; }

                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    radnikLK.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    radnikLK.id = "";
                }

                // KREIRANJE CVORA RADNIK LK
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:RadnikLicnaKarta {jmbg:'" + radnikLK.jmbg + "'})"
                                                                                                            + " set n.ime = '" + radnikLK.ime
                                                                                                            + "', n.prezime = '" + radnikLK.prezime
                                                                                                            + "', n.jmbg = '" + radnikLK.jmbg
                                                                                                            + "', n.datumRodjenja = '" + radnikLK.datumRodjenja
                                                                                                            + "', n.smena = '" + radnikLK.smena
                                                                                                            + "' return n",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);

                List<RadnikLicnaKarta> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikLicnaKarta r in radnici)
                {
                    MessageBox.Show("Radnik sa saltera LICNE KARTE: " + r.ime + " " + r.prezime + " je uspesno izmenjen!");
                }

                this.Close();
            }
            else if (tip == 2)
            {
                radnikP.ime = txbUnosIme.Text;
                radnikP.prezime = txbUnosPrezime.Text;
                radnikP.jmbg = txbUnosJMBG.Text;
                radnikP.datumRodjenja = dtpDatumRodjenja.Text;
                if (rdbPrva.Checked == true) { radnikP.smena = "Prva"; }
                else if (rdbDruga.Checked == true) { radnikP.smena = "Druga"; }

                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    radnikP.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    radnikP.id = "";
                }

                // KREIRANJE CVORA RADNIK P
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:RadnikPasos {jmbg:'" + radnikP.jmbg + "'})"
                                                                                                            + " set n.ime = '" + radnikP.ime
                                                                                                            + "', n.prezime = '" + radnikP.prezime
                                                                                                            + "', n.jmbg = '" + radnikP.jmbg
                                                                                                            + "', n.datumRodjenja = '" + radnikP.datumRodjenja
                                                                                                            + "', n.smena = '" + radnikP.smena
                                                                                                            + "' return n",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);

                List<RadnikPasos> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikPasos r in radnici)
                {
                    MessageBox.Show("Radnik sa saltera PASOSI: " + r.ime + " " + r.prezime + " je uspesno izmenjen!");
                }

                this.Close();
            }
            else if (tip == 3)
            {
                radnikRV.ime = txbUnosIme.Text;
                radnikRV.prezime = txbUnosPrezime.Text;
                radnikRV.jmbg = txbUnosJMBG.Text;
                radnikRV.datumRodjenja = dtpDatumRodjenja.Text;
                if (rdbPrva.Checked == true) { radnikRV.smena = "Prva"; }
                else if (rdbDruga.Checked == true) { radnikRV.smena = "Druga"; }

                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    radnikRV.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    radnikRV.id = "";
                }

                // KREIRANJE CVORA RADNIK RV
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:RadnikRegistracijaVozila {jmbg:'" + radnikRV.jmbg + "'})"
                                                                                                            + " set n.ime = '" + radnikRV.ime
                                                                                                            + "', n.prezime = '" + radnikRV.prezime
                                                                                                            + "', n.jmbg = '" + radnikRV.jmbg
                                                                                                            + "', n.datumRodjenja = '" + radnikRV.datumRodjenja
                                                                                                            + "', n.smena = '" + radnikRV.smena
                                                                                                            + "' return n",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);

                List<RadnikRegistracijaVozila> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikRegistracijaVozila r in radnici)
                {
                    MessageBox.Show("Radnik sa saltera REGISTRACIJE VOZILA: " + r.ime + " " + r.prezime + " je uspesno izmenjen!");
                }

                this.Close();
            }
            else if (tip == 4)
            {
                radnikSK.ime = txbUnosIme.Text;
                radnikSK.prezime = txbUnosPrezime.Text;
                radnikSK.jmbg = txbUnosJMBG.Text;
                radnikSK.datumRodjenja = dtpDatumRodjenja.Text;
                if (rdbPrva.Checked == true) { radnikSK.smena = "Prva"; }
                else if (rdbDruga.Checked == true) { radnikSK.smena = "Druga"; }

                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    radnikSK.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    radnikSK.id = "";
                }

                // KREIRANJE CVORA RADNIK SK
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:RadnikSaobracajniPolicajac {jmbg:'" + radnikSK.jmbg + "'})"
                                                                                                            + " set n.ime = '" + radnikSK.ime
                                                                                                            + "', n.prezime = '" + radnikSK.prezime
                                                                                                            + "', n.jmbg = '" + radnikSK.jmbg
                                                                                                            + "', n.datumRodjenja = '" + radnikSK.datumRodjenja
                                                                                                            + "', n.smena = '" + radnikSK.smena
                                                                                                            + "' return n",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);

                List<RadnikSaobracajniPolicajac> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikSaobracajniPolicajac r in radnici)
                {
                    MessageBox.Show("Radnik sa saltera SAOBRACAJNE KAZNE: " + r.ime + " " + r.prezime + " je uspesno izmenjen!");
                }

                this.Close();
            }
            else if (tip == 5)
            {
                radnikVD.ime = txbUnosIme.Text;
                radnikVD.prezime = txbUnosPrezime.Text;
                radnikVD.jmbg = txbUnosJMBG.Text;
                radnikVD.datumRodjenja = dtpDatumRodjenja.Text;
                if (rdbPrva.Checked == true) { radnikVD.smena = "Prva"; }
                else if (rdbDruga.Checked == true) { radnikVD.smena = "Druga"; }

                string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    radnikVD.id = (mId++).ToString();
                }
                catch (Exception exception)
                {
                    radnikVD.id = "";
                }

                // KREIRANJE CVORA RADNIK VD
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:RadnikVozackaDozvola {jmbg:'" + radnikVD.jmbg + "'})"
                                                                                                            + " set n.ime = '" + radnikVD.ime
                                                                                                            + "', n.prezime = '" + radnikVD.prezime
                                                                                                            + "', n.jmbg = '" + radnikVD.jmbg
                                                                                                            + "', n.datumRodjenja = '" + radnikVD.datumRodjenja
                                                                                                            + "', n.smena = '" + radnikVD.smena
                                                                                                            + "' return n",
                                                                new Dictionary<string, object>(), CypherResultMode.Set);

                List<RadnikVozackaDozvola> radnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

                // ISPIS KREIRANOG REZULTATA
                foreach (RadnikVozackaDozvola r in radnici)
                {
                    MessageBox.Show("Radnik sa saltera VOZACKE DOZVOLE: " + r.ime + " " + r.prezime + " je uspesno izmenjen!");
                }

                this.Close();
            }
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
