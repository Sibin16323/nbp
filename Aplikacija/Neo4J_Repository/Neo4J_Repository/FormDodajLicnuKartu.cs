﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajLicnuKartu : Form
    {
        public GraphClient client;
        List<Korisnik> sviKorisnici;
        List<RadnikLicnaKarta> sviRadnici;
        LicnaKarta dokument;
        public FormDodajLicnuKartu()
        {
            InitializeComponent();

        }

        private void FormDodajLicnuKartu_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_LK]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            foreach (Korisnik k in sviKorisnici)
            {
                this.cbxKorisnici.Items.Add(k.ime + " " + k.prezime + " JBMG: " + k.jmbg);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikLicnaKarta) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query2).ToList();

            foreach (RadnikLicnaKarta r in sviRadnici)
            {
                this.cbxRadnici.Items.Add(r.ime + " " + r.prezime);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if(cbxRadnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati radnika koji ce izdati licnu kartu!");
                return;
            }

            if (cbxKorisnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati korisnika kome ce biti izdata licna karta!");
                return;
            }

            if (txbUnosBroj.Text == "")
            {
                MessageBox.Show("Niste uneli broj licne karte!");
                return;
            }

            if (txbUnosBroj.TextLength < 9)
            {
                MessageBox.Show("Broj licne karte mora imati 9 cifara!");
                return;
            }

            if (txbUnosPU.Text == "")
            {
                MessageBox.Show("Niste uneli naziv PU jedinice (Primer: 'PU u Nisu') !");
                return;
            }

            if(rbM.Checked == false && rbZ.Checked == false)
            {
                MessageBox.Show("Niste odabrali pol !");
                return;
            }

            LicnaKarta licnaKarta = this.kreirajLicnuKartu();
            IzdaoLK izdao = this.izdaoLicnuKartu();
            PosedujeLK poseduje = this.posedujeLicnuKartu();
            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                licnaKarta.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                licnaKarta.id = "";
            }

            string polM = "M";
            string polZ = "Z";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojLK", txbUnosBroj.Text);
            queryDict.Add("izdataOd", txbUnosPU.Text);
            queryDict.Add("vaziOd", dtpDatumOd.Value.ToString());
            queryDict.Add("vaziDo", dtpDatumDo.Value.ToString());
            if(rbM.Checked)
            {
                queryDict.Add("pol", polM);
            }
            else if(rbZ.Checked)
            {
                queryDict.Add("pol", polZ);
            }

            // SELEKTOVAN KORISNIK
            Korisnik kor = sviKorisnici[cbxKorisnici.SelectedIndex];

            // SELEKTOVAN RADNIK
            RadnikLicnaKarta rad = sviRadnici[cbxRadnici.SelectedIndex];

            // KREIRANJE CVORA LICNA KARTA
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:LicnaKarta {brojLK:'" + licnaKarta.brojLK
                                                            + "', izdataOd:'" + licnaKarta.izdataOd 
                                                            + "', vaziOd:'" + licnaKarta.vaziOd
                                                            + "', vaziDo:'" + licnaKarta.vaziDo 
                                                            + "', pol:'" + licnaKarta.pol
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<LicnaKarta> licneKarte = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            // KREIRANJE RELACIJE KORISNIK -> POSEDUJE
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Korisnik {jmbg:'" + kor.jmbg + "'}), (b:LicnaKarta {brojLK:'" + licnaKarta.brojLK + "'}) MERGE (a)-[r:POSEDUJE_LK]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<PosedujeLK> relacije = ((IRawGraphClient)client).ExecuteGetCypherResults<PosedujeLK>(query2).ToList();

            // KREIRANJE RELACIJE RADNIK -> IZDAO
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:RadnikLicnaKarta {jmbg:'" + rad.jmbg + "'}), (b:LicnaKarta {brojLK:'" + licnaKarta.brojLK + "'}) MERGE (a)-[r:IZDAO_LK]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<IzdaoLK> relacije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<IzdaoLK>(query3).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (LicnaKarta lk in licneKarte)
            {
                MessageBox.Show("Licna karta sa brojem: " + lk.brojLK + " je uspesno kreirana za korisnika " + kor.ime + " " + kor.prezime + "!");
            }
            
            this.Close();
        }

        private LicnaKarta kreirajLicnuKartu()
        {
            dokument = new LicnaKarta();

            dokument.brojLK = txbUnosBroj.Text;
            dokument.izdataOd = txbUnosPU.Text;
            dokument.vaziOd = dtpDatumOd.Value.Date.ToString("dd.MM.yyyy.");
            dokument.vaziDo = dtpDatumDo.Value.Date.ToString("dd.MM.yyyy.");
            if (rbM.Checked == true)
                dokument.pol = "M";
            else
                dokument.pol = "Z";

            return dokument;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private IzdaoLK izdaoLicnuKartu()
        {
            IzdaoLK lk = new IzdaoLK();
            
            RadnikLicnaKarta radnik2 = sviRadnici[cbxRadnici.SelectedIndex];

            lk.radnik = radnik2;
            lk.dokument = this.dokument;

            return lk;
        }

        private PosedujeLK posedujeLicnuKartu()
        {
            PosedujeLK lk = new PosedujeLK();

            Korisnik korisnik2 = sviKorisnici[cbxKorisnici.SelectedIndex];

            lk.korisnik = korisnik2;
            lk.licnaKarta = this.dokument;

            return lk;
        }
    }
}
