﻿namespace Neo4J_Repository
{
    partial class FormRegistracijeVozila
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPoKategoriji = new System.Windows.Forms.Button();
            this.txbKategorija = new System.Windows.Forms.TextBox();
            this.btnSveRegistracijeVozila = new System.Windows.Forms.Button();
            this.btnPoDatumuVazenja = new System.Windows.Forms.Button();
            this.btnPoRegistarskomBroju = new System.Windows.Forms.Button();
            this.txbRegistarskiBroj = new System.Windows.Forms.TextBox();
            this.txbDatumVazenja = new System.Windows.Forms.TextBox();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.registracijeVozila = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPoKategoriji
            // 
            this.btnPoKategoriji.Location = new System.Drawing.Point(174, 110);
            this.btnPoKategoriji.Name = "btnPoKategoriji";
            this.btnPoKategoriji.Size = new System.Drawing.Size(281, 32);
            this.btnPoKategoriji.TabIndex = 5;
            this.btnPoKategoriji.Text = "Pretrazi registracije po kategoriji";
            this.btnPoKategoriji.UseVisualStyleBackColor = true;
            this.btnPoKategoriji.Click += new System.EventHandler(this.btnPoKategoriji_Click);
            // 
            // txbKategorija
            // 
            this.txbKategorija.Location = new System.Drawing.Point(7, 115);
            this.txbKategorija.Name = "txbKategorija";
            this.txbKategorija.Size = new System.Drawing.Size(149, 22);
            this.txbKategorija.TabIndex = 4;
            // 
            // btnSveRegistracijeVozila
            // 
            this.btnSveRegistracijeVozila.Location = new System.Drawing.Point(461, 22);
            this.btnSveRegistracijeVozila.Name = "btnSveRegistracijeVozila";
            this.btnSveRegistracijeVozila.Size = new System.Drawing.Size(167, 33);
            this.btnSveRegistracijeVozila.TabIndex = 6;
            this.btnSveRegistracijeVozila.Text = "Sve registracija vozila";
            this.btnSveRegistracijeVozila.UseVisualStyleBackColor = true;
            this.btnSveRegistracijeVozila.Click += new System.EventHandler(this.btnSveRegistracijeVozila_Click);
            // 
            // btnPoDatumuVazenja
            // 
            this.btnPoDatumuVazenja.Location = new System.Drawing.Point(174, 66);
            this.btnPoDatumuVazenja.Name = "btnPoDatumuVazenja";
            this.btnPoDatumuVazenja.Size = new System.Drawing.Size(281, 33);
            this.btnPoDatumuVazenja.TabIndex = 3;
            this.btnPoDatumuVazenja.Text = "Pretrazi registracije po datumu vazenja";
            this.btnPoDatumuVazenja.UseVisualStyleBackColor = true;
            this.btnPoDatumuVazenja.Click += new System.EventHandler(this.btnPoDatumuVazenja_Click);
            // 
            // btnPoRegistarskomBroju
            // 
            this.btnPoRegistarskomBroju.Location = new System.Drawing.Point(174, 22);
            this.btnPoRegistarskomBroju.Name = "btnPoRegistarskomBroju";
            this.btnPoRegistarskomBroju.Size = new System.Drawing.Size(281, 33);
            this.btnPoRegistarskomBroju.TabIndex = 1;
            this.btnPoRegistarskomBroju.Text = "Pretrazi registracije po registarskom broju";
            this.btnPoRegistarskomBroju.UseVisualStyleBackColor = true;
            this.btnPoRegistarskomBroju.Click += new System.EventHandler(this.btnPoRegistarskomBroju_Click);
            // 
            // txbRegistarskiBroj
            // 
            this.txbRegistarskiBroj.Location = new System.Drawing.Point(7, 27);
            this.txbRegistarskiBroj.Name = "txbRegistarskiBroj";
            this.txbRegistarskiBroj.Size = new System.Drawing.Size(149, 22);
            this.txbRegistarskiBroj.TabIndex = 0;
            // 
            // txbDatumVazenja
            // 
            this.txbDatumVazenja.Location = new System.Drawing.Point(7, 71);
            this.txbDatumVazenja.Name = "txbDatumVazenja";
            this.txbDatumVazenja.Size = new System.Drawing.Size(149, 22);
            this.txbDatumVazenja.TabIndex = 2;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Kategorija";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vazi do";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 120;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Registarski broj";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 120;
            // 
            // registracijeVozila
            // 
            this.registracijeVozila.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.registracijeVozila.Dock = System.Windows.Forms.DockStyle.Fill;
            this.registracijeVozila.FullRowSelect = true;
            this.registracijeVozila.GridLines = true;
            this.registracijeVozila.Location = new System.Drawing.Point(4, 19);
            this.registracijeVozila.Margin = new System.Windows.Forms.Padding(4);
            this.registracijeVozila.Name = "registracijeVozila";
            this.registracijeVozila.Size = new System.Drawing.Size(631, 178);
            this.registracijeVozila.TabIndex = 1;
            this.registracijeVozila.UseCompatibleStateImageBehavior = false;
            this.registracijeVozila.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnObrisi);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.btnPoKategoriji);
            this.groupBox1.Controls.Add(this.txbKategorija);
            this.groupBox1.Controls.Add(this.btnSveRegistracijeVozila);
            this.groupBox1.Controls.Add(this.btnPoDatumuVazenja);
            this.groupBox1.Controls.Add(this.btnPoRegistarskomBroju);
            this.groupBox1.Controls.Add(this.txbRegistarskiBroj);
            this.groupBox1.Controls.Add(this.txbDatumVazenja);
            this.groupBox1.Location = new System.Drawing.Point(14, 220);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(639, 152);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(461, 109);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(167, 33);
            this.btnObrisi.TabIndex = 8;
            this.btnObrisi.Text = "Obrisi registraciju vozila";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(461, 66);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(167, 33);
            this.btnDodaj.TabIndex = 7;
            this.btnDodaj.Text = "Dodaj registraciju vozila";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.registracijeVozila);
            this.groupBox2.Location = new System.Drawing.Point(14, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(639, 201);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista registracija vozila";
            // 
            // FormRegistracijeVozila
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 383);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MaximumSize = new System.Drawing.Size(685, 430);
            this.MinimumSize = new System.Drawing.Size(685, 430);
            this.Name = "FormRegistracijeVozila";
            this.Text = "REGISTRACIJE VOZILA";
            this.Load += new System.EventHandler(this.FormRegistracijeVozila_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPoKategoriji;
        private System.Windows.Forms.TextBox txbKategorija;
        private System.Windows.Forms.Button btnSveRegistracijeVozila;
        private System.Windows.Forms.Button btnPoDatumuVazenja;
        private System.Windows.Forms.Button btnPoRegistarskomBroju;
        private System.Windows.Forms.TextBox txbRegistarskiBroj;
        private System.Windows.Forms.TextBox txbDatumVazenja;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView registracijeVozila;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnObrisi;
    }
}