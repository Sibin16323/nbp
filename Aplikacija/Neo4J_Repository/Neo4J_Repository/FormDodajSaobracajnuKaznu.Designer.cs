﻿namespace Neo4J_Repository
{
    partial class FormDodajSaobracajnuKaznu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.txbUnosCena = new System.Windows.Forms.TextBox();
            this.cbxKorisnici = new System.Windows.Forms.ComboBox();
            this.cbxRadnici = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDatumOd = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txbUnosRazlog = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 203);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 17);
            this.label9.TabIndex = 28;
            this.label9.Text = "Unesite cenu kazne:";
            // 
            // txbUnosCena
            // 
            this.txbUnosCena.Location = new System.Drawing.Point(188, 200);
            this.txbUnosCena.MaxLength = 100;
            this.txbUnosCena.Name = "txbUnosCena";
            this.txbUnosCena.Size = new System.Drawing.Size(100, 22);
            this.txbUnosCena.TabIndex = 4;
            // 
            // cbxKorisnici
            // 
            this.cbxKorisnici.FormattingEnabled = true;
            this.cbxKorisnici.Location = new System.Drawing.Point(188, 80);
            this.cbxKorisnici.Name = "cbxKorisnici";
            this.cbxKorisnici.Size = new System.Drawing.Size(301, 24);
            this.cbxKorisnici.TabIndex = 1;
            // 
            // cbxRadnici
            // 
            this.cbxRadnici.FormattingEnabled = true;
            this.cbxRadnici.Location = new System.Drawing.Point(188, 37);
            this.cbxRadnici.Name = "cbxRadnici";
            this.cbxRadnici.Size = new System.Drawing.Size(301, 24);
            this.cbxRadnici.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Izaberite radnika:";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(380, 124);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(109, 98);
            this.btnDodaj.TabIndex = 5;
            this.btnDodaj.Text = "Dodaj saobracajnu kaznu";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Izaberite korisnika:";
            // 
            // dtpDatumOd
            // 
            this.dtpDatumOd.CustomFormat = "dd.MM.yyyy.";
            this.dtpDatumOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumOd.Location = new System.Drawing.Point(188, 124);
            this.dtpDatumOd.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDatumOd.Name = "dtpDatumOd";
            this.dtpDatumOd.Size = new System.Drawing.Size(160, 22);
            this.dtpDatumOd.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Unesite datum izdavanja:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txbUnosCena);
            this.groupBox1.Controls.Add(this.txbUnosRazlog);
            this.groupBox1.Controls.Add(this.cbxKorisnici);
            this.groupBox1.Controls.Add(this.cbxRadnici);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpDatumOd);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(508, 234);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodavanje saobracajne kazne";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 166);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 17);
            this.label8.TabIndex = 27;
            this.label8.Text = "Unesite razlog kazne:";
            // 
            // txbUnosRazlog
            // 
            this.txbUnosRazlog.Location = new System.Drawing.Point(188, 163);
            this.txbUnosRazlog.MaxLength = 100;
            this.txbUnosRazlog.Name = "txbUnosRazlog";
            this.txbUnosRazlog.Size = new System.Drawing.Size(100, 22);
            this.txbUnosRazlog.TabIndex = 3;
            // 
            // FormDodajSaobracajnuKaznu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 253);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(550, 300);
            this.MinimumSize = new System.Drawing.Size(550, 300);
            this.Name = "FormDodajSaobracajnuKaznu";
            this.Text = "DODAVANJE SAOBRACAJNE KAZNE";
            this.Load += new System.EventHandler(this.FormDodajSaobracajnuKaznu_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txbUnosCena;
        private System.Windows.Forms.ComboBox cbxKorisnici;
        private System.Windows.Forms.ComboBox cbxRadnici;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDatumOd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbUnosRazlog;
    }
}