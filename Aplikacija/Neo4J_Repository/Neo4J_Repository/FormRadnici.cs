﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormRadnici : Form
    {
        public GraphClient client;
        public int trRadnici = 0;
        public FormRadnici()
        {
            InitializeComponent();
        }

        private void btnSaLK_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikLicnaKarta) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RadnikLicnaKarta> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();

            this.radnici.Items.Clear();

            foreach (RadnikLicnaKarta r in sviRadnici)
            {
                ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                this.radnici.Items.Add(item);
            }

            this.radnici.Refresh();

            trRadnici = 1; // LK
        }

        private void btnSaP_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikPasos) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RadnikPasos> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

            this.radnici.Items.Clear();

            foreach (RadnikPasos r in sviRadnici)
            {
                ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                this.radnici.Items.Add(item);
            }

            this.radnici.Refresh();

            trRadnici = 2; // P
        }

        private void btnSaRV_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikRegistracijaVozila) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RadnikRegistracijaVozila> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

            this.radnici.Items.Clear();

            foreach (RadnikRegistracijaVozila r in sviRadnici)
            {
                ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                this.radnici.Items.Add(item);
            }

            this.radnici.Refresh();

            trRadnici = 3; // RV
        }

        private void btnSaSK_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikSaobracajniPolicajac) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RadnikSaobracajniPolicajac> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

            this.radnici.Items.Clear();

            foreach (RadnikSaobracajniPolicajac r in sviRadnici)
            {
                ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                this.radnici.Items.Add(item);
            }

            this.radnici.Refresh();

            trRadnici = 4; // SK
        }

        private void btnSaVD_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikVozackaDozvola) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<RadnikVozackaDozvola> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

            this.radnici.Items.Clear();

            foreach (RadnikVozackaDozvola r in sviRadnici)
            {
                ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                this.radnici.Items.Add(item);
            }

            this.radnici.Refresh();

            trRadnici = 5; // VD
        }

        private void btnPoImenu_Click(object sender, EventArgs e)
        {
            if (txbIme.Text == "")
            {
                MessageBox.Show("Niste uneli ime ili deo imena radnika!");
                return;
            }

            string radnikIme = ".*" + txbIme.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("radnikIme", radnikIme);

            if (trRadnici == 1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikLicnaKarta) and exists(n.ime) and n.ime =~ {radnikIme} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikLicnaKarta> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();
    
                this.radnici.Items.Clear();
    
                foreach (RadnikLicnaKarta r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 2)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikPasos) and exists(n.ime) and n.ime =~ {radnikIme} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikPasos> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikPasos r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 3)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikRegistracijaVozila) and exists(n.ime) and n.ime =~ {radnikIme} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikRegistracijaVozila> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikRegistracijaVozila r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 4)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikSaobracajniPolicajac) and exists(n.ime) and n.ime =~ {radnikIme} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikSaobracajniPolicajac> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikSaobracajniPolicajac r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 5)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikVozackaDozvola) and exists(n.ime) and n.ime =~ {radnikIme} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikVozackaDozvola> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikVozackaDozvola r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }

            this.radnici.Refresh();
        }

        private void btnPoJMBG_Click(object sender, EventArgs e)
        {
            if (txbJmbg.Text == "")
            {
                MessageBox.Show("Niste uneli JMBG ili deo JMBG-a radnika!");
                return;
            }

            string radnikJMBG = ".*" + txbJmbg.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("radnikJMBG", radnikJMBG);

            if (trRadnici == 1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikLicnaKarta) and exists(n.jmbg) and n.jmbg =~ {radnikJMBG} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikLicnaKarta> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikLicnaKarta r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 2)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikPasos) and exists(n.jmbg) and n.jmbg =~ {radnikJMBG} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikPasos> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikPasos r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 3)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikRegistracijaVozila) and exists(n.jmbg) and n.jmbg =~ {radnikJMBG} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikRegistracijaVozila> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikRegistracijaVozila r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 4)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikSaobracajniPolicajac) and exists(n.jmbg) and n.jmbg =~ {radnikJMBG} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikSaobracajniPolicajac> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikSaobracajniPolicajac r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 5)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikVozackaDozvola) and exists(n.jmbg) and n.jmbg =~ {radnikJMBG} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikVozackaDozvola> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikVozackaDozvola r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }

            this.radnici.Refresh();
        }

        private void btnPoDatumuRodjenja_Click(object sender, EventArgs e)
        {
            if (txbDatumRodjenja.Text == "")
            {
                MessageBox.Show("Niste uneli datum ili deo datuma rodjenja radnika!");
                return;
            }

            string radnikDatumRodjenja = ".*" + txbDatumRodjenja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("radnikDatumRodjenja", radnikDatumRodjenja);

            if (trRadnici == 1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikLicnaKarta) and exists(n.datumRodjenja) and n.datumRodjenja =~ {radnikDatumRodjenja} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikLicnaKarta> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikLicnaKarta r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 2)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikPasos) and exists(n.datumRodjenja) and n.datumRodjenja =~ {radnikDatumRodjenja} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikPasos> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikPasos r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 3)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikRegistracijaVozila) and exists(n.datumRodjenja) and n.datumRodjenja =~ {radnikDatumRodjenja} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikRegistracijaVozila> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikRegistracijaVozila r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 4)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikSaobracajniPolicajac) and exists(n.datumRodjenja) and n.datumRodjenja =~ {radnikDatumRodjenja} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikSaobracajniPolicajac> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikSaobracajniPolicajac r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 5)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikVozackaDozvola) and exists(n.datumRodjenja) and n.datumRodjenja =~ {radnikDatumRodjenja} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikVozackaDozvola> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikVozackaDozvola r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }

            this.radnici.Refresh();
        }

        private void btnPoSmeni_Click(object sender, EventArgs e)
        {
            if (txbSmena.Text == "")
            {
                MessageBox.Show("Niste uneli smenu radnika (Primer: 'Prva' ili 'Druga')!");
                return;
            }

            string radnikSmena = ".*" + txbSmena.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("radnikSmena", radnikSmena);

            if (trRadnici == 1)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikLicnaKarta) and exists(n.smena) and n.smena =~ {radnikSmena} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikLicnaKarta> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikLicnaKarta>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikLicnaKarta r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 2)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikPasos) and exists(n.smena) and n.smena =~ {radnikSmena} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikPasos> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikPasos r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 3)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikRegistracijaVozila) and exists(n.smena) and n.smena =~ {radnikSmena} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikRegistracijaVozila> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikRegistracijaVozila>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikRegistracijaVozila r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 4)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikSaobracajniPolicajac) and exists(n.smena) and n.smena =~ {radnikSmena} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikSaobracajniPolicajac> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikSaobracajniPolicajac r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }
            else if (trRadnici == 5)
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:RadnikVozackaDozvola) and exists(n.smena) and n.smena =~ {radnikSmena} return n",
                                                                    queryDict, CypherResultMode.Set);

                List<RadnikVozackaDozvola> sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query).ToList();

                this.radnici.Items.Clear();

                foreach (RadnikVozackaDozvola r in sviRadnici)
                {
                    ListViewItem item = new ListViewItem(new string[] { r.ime, r.prezime, r.jmbg, r.datumRodjenja, r.smena });
                    this.radnici.Items.Add(item);
                }
            }

            this.radnici.Refresh();
        }

        private void btnPrikaziLicneKarte_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika ciju listu licnih karata koje je izdao zelite da prikazete!");
                return;
            }

            if (trRadnici == 1)
            {
                string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("radnikJmbg", radnikJmbg);

                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikLicnaKarta{jmbg:{radnikJmbg}})-[:IZDAO_LK]->(b:LicnaKarta) return b",
                                                                        queryDict, CypherResultMode.Set);

                List<LicnaKarta> licna = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

                foreach (LicnaKarta lk in licna)
                {
                    MessageBox.Show("Broj licne karte: " + lk.brojLK + "\nIzdata od: " + lk.izdataOd + "\nVazi od: " + lk.vaziOd + "\nVazi do: " + lk.vaziDo + "\nPol: " + lk.pol);
                }
            }
            else
            {
                MessageBox.Show("Izabrani radnik nije radnik sa saltera za LICNE KARTE!\n(Odaberite prvo opciju za prikaz zeljenih radnika)");
            }
        }

        private void btnPrikaziPasose_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika ciju listu pasosa koje je izdao zelite da prikazete!");
                return;
            }

            if (trRadnici == 2)
            {
                string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("radnikJmbg", radnikJmbg);

                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikPasos{jmbg:{radnikJmbg}})-[:IZDAO_P]->(b:Pasos) return b",
                                                                        queryDict, CypherResultMode.Set);

                List<Pasos> pasos = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

                foreach (Pasos p in pasos)
                {
                    MessageBox.Show("Broj pasosa: " + p.brojPasosa + "\nIzdat od: " + p.izdatOd + "\nVazi od: " + p.vaziOd + "\nVazi do: " + p.vaziDo + "\nPol: " + p.pol + "\nDrzavljanstvo: " + p.drzavljanstvo + "\nPrebivaliste: " + p.prebivaliste);
                }
            }
            else
            {
                MessageBox.Show("Izabrani radnik nije radnik sa saltera za PASOSE!\n(Odaberite prvo opciju za prikaz zeljenih radnika)");
            }
        }

        private void btnPrikaziRegistracije_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika ciju listu registracija vozila koje je izdao zelite da prikazete!");
                return;
            }

            if (trRadnici == 3)
            {
                string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("radnikJmbg", radnikJmbg);

                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikRegistracijaVozila{jmbg:{radnikJmbg}})-[:IZDAO_RV]->(b:RegistracijaVozila) return b",
                                                                        queryDict, CypherResultMode.Set);

                List<RegistracijaVozila> registracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

                foreach (RegistracijaVozila r in registracije)
                {
                    MessageBox.Show("Registarski broj: " + r.registarskiBroj + "\nVazi do: " + r.vaziDo + "\nKategorija: " + r.kategorija);
                }
            }
            else
            {
                MessageBox.Show("Izabrani radnik nije radnik sa saltera za REGISTRACIJE VOZILA!\n(Odaberite prvo opciju za prikaz zeljenih radnika)");
            }
        }

        private void btnPrikaziKazne_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika ciju listu saobracajnih kazni koje je izdao zelite da prikazete!");
                return;
            }

            if (trRadnici == 4)
            {
                string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("radnikJmbg", radnikJmbg);

                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikSaobracajniPolicajac{jmbg:{radnikJmbg}})-[:IZDAO_SK]->(b:SaobracajnaKazna) return b",
                                                                        queryDict, CypherResultMode.Set);

                List<SaobracajnaKazna> kazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

                foreach (SaobracajnaKazna k in kazne)
                {
                    MessageBox.Show("Datum kazne: " + k.datumKazne + "\nRazlog: " + k.razlog + "\nCena: " + k.cena);
                }
            }
            else
            {
                MessageBox.Show("Izabrani radnik nije radnik sa saltera za SAOBRACAJNE KAZNE!\n(Odaberite prvo opciju za prikaz zeljenih radnika)");
            }
        }

        private void btnPrikaziVozacke_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika ciju listu vozackih dozvola koje je izdao zelite da prikazete!");
                return;
            }

            if (trRadnici == 5)
            {
                string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("radnikJmbg", radnikJmbg);

                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikVozackaDozvola{jmbg:{radnikJmbg}})-[:IZDAO_VD]->(b:VozackaDozvola) return b",
                                                                        queryDict, CypherResultMode.Set);

                List<VozackaDozvola> vozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

                foreach (VozackaDozvola v in vozacke)
                {
                    MessageBox.Show("Broj vozacke dozvole: " + v.brojVD + "\nIzdata od: " + v.izdataOd + "\nVazi od: " + v.vaziOd + "\nVazi do: " + v.vaziDo + "\nKategorije: " + v.kategorije);
                }
            }
            else
            {
                MessageBox.Show("Izabrani radnik nije radnik sa saltera za VOZACKE DOZVOLE!\n(Odaberite prvo opciju za prikaz zeljenih radnika)");
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormDodajRadnika DodajRadnikaForm = new FormDodajRadnika();
            DodajRadnikaForm.client = client;
            DodajRadnikaForm.ShowDialog();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika koga zelite da izmenite!");
                return;
            }

            string radnikIme = radnici.SelectedItems[0].SubItems[0].Text;
            string radnikPrezime = radnici.SelectedItems[0].SubItems[1].Text;
            string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;
            string radnikDatumRodjenja = radnici.SelectedItems[0].SubItems[3].Text;
            string radnikSmena = radnici.SelectedItems[0].SubItems[4].Text;
            int radnikTip = trRadnici;

            FormIzmeniRadnika IzmeniRadnikaForm = new FormIzmeniRadnika(radnikIme, radnikPrezime, radnikJmbg, radnikDatumRodjenja, radnikSmena, radnikTip);
            IzmeniRadnikaForm.client = client;
            IzmeniRadnikaForm.ShowDialog();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (radnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite radnika koga zelite da obrisete!");
                return;
            }

            string radnikJmbg = radnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("radnikJmbg", radnikJmbg);

            if (trRadnici == 1)
            {
                // PRVO BRISEMO RELACIJE
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikLicnaKarta{jmbg:{radnikJmbg}})-[r]-(b) delete r",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);

                // DRUGO BRISEMO RADNIKA
                var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikLicnaKarta{jmbg:{radnikJmbg}}) delete n",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query2);

                MessageBox.Show("Radnik sa saltera LICNE KARTE je uspesno obrisan sa svim njegovim relacijama!");
            }
            else if (trRadnici == 2)
            {
                // PRVO BRISEMO RELACIJE
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikPasos{jmbg:{radnikJmbg}})-[r]-(b) delete r",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);

                // DRUGO BRISEMO RADNIKA
                var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikPasos{jmbg:{radnikJmbg}}) delete n",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query2);

                MessageBox.Show("Radnik sa saltera PASOSI je uspesno obrisan sa svim njegovim relacijama!");
            }
            else if (trRadnici == 3)
            {
                // PRVO BRISEMO RELACIJE
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikRegistracijaVozila{jmbg:{radnikJmbg}})-[r]-(b) delete r",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);

                // DRUGO BRISEMO RADNIKA
                var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikRegistracijaVozila{jmbg:{radnikJmbg}}) delete n",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query2);

                MessageBox.Show("Radnik sa saltera REGISTRACIJE VOZILA je uspesno obrisan sa svim njegovim relacijama!");
            }
            else if (trRadnici == 4)
            {
                // PRVO BRISEMO RELACIJE
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikSaobracajniPolicajac{jmbg:{radnikJmbg}})-[r]-(b) delete r",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);

                // DRUGO BRISEMO RADNIKA
                var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikSaobracajniPolicajac{jmbg:{radnikJmbg}}) delete n",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query2);

                MessageBox.Show("Radnik sa saltera SAOBRACAJNE KAZNE je uspesno obrisan sa svim njegovim relacijama!");
            }
            else if (trRadnici == 5)
            {
                // PRVO BRISEMO RELACIJE
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikVozackaDozvola{jmbg:{radnikJmbg}})-[r]-(b) delete r",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);

                // DRUGO BRISEMO RADNIKA
                var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikVozackaDozvola{jmbg:{radnikJmbg}}) delete n",
                                                                        queryDict, CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query2);

                MessageBox.Show("Radnik sa saltera VOZACKE DOZVOLE je uspesno obrisan sa svim njegovim relacijama!");
            }
        }
    }
}
