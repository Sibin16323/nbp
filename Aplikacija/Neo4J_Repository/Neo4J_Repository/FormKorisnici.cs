﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormKorisnici : Form
    {
        public GraphClient client;
        List<Korisnik> sviKorisnici;
        public FormKorisnici()
        {
            InitializeComponent();
        }

        private void btnSaLK_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n)-[r:POSEDUJE_LK]->(licnakarta) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnSaP_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n)-[r:POSEDUJE_P]->(pasos) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnSaRV_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n)-[r:POSEDUJE_RV]->(regvozilo) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnSaSK_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n)-[r:POSEDUJE_SK]->(saobkazna) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnSaVD_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n)-[r:POSEDUJE_VD]->(vozackadozvola) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnBezLK_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_LK]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnBezP_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_P]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnBezRV_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_RV]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnBezSK_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_SK]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnBezVD_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_VD]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnPoImenu_Click(object sender, EventArgs e)
        {
            if(txbIme.Text == "")
            {
                MessageBox.Show("Niste uneli ime ili deo imena korisnika!");
                return;
            }

            string korisnikIme = ".*" + txbIme.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikIme", korisnikIme);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Korisnik) and exists(n.ime) and n.ime =~ {korisnikIme} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnPoJMBG_Click(object sender, EventArgs e)
        {
            if (txbJmbg.Text == "")
            {
                MessageBox.Show("Niste uneli JMBG korisnika!");
                return;
            }

            string korisnikJmbg = ".*" + txbJmbg.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Korisnik) and exists(n.jmbg) and n.jmbg =~ {korisnikJmbg} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnPoMestuRodjenja_Click(object sender, EventArgs e)
        {
            if (txbMestoRodjenja.Text == "")
            {
                MessageBox.Show("Niste uneli mesto rodjenja korisnika!");
                return;
            }

            string korisnikMestoRodjenja = ".*" + txbMestoRodjenja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikMestoRodjenja", korisnikMestoRodjenja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Korisnik) and exists(n.mestoRodjenja) and n.mestoRodjenja =~ {korisnikMestoRodjenja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Korisnik> sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void FormKorisnici_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            this.korisnici.Items.Clear();

            foreach (Korisnik k in sviKorisnici)
            {
                ListViewItem item = new ListViewItem(new string[] { k.ime, k.prezime, k.jmbg, k.datumRodjenja, k.mestoRodjenja });
                this.korisnici.Items.Add(item);
            }

            this.korisnici.Refresh();
        }

        private void btnPrikaziLicnuKartu_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika ciju licnu kartu zelite da prikazete!");
                return;
            }

            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}})-[:POSEDUJE_LK]->(b:LicnaKarta) return b",
                                                                    queryDict, CypherResultMode.Set);

            List<LicnaKarta> licna = ((IRawGraphClient)client).ExecuteGetCypherResults<LicnaKarta>(query).ToList();

            foreach (LicnaKarta lk in licna)
            {
                MessageBox.Show("Broj licne karte: " + lk.brojLK + "\nIzdata od: " + lk.izdataOd + "\nVazi od: " + lk.vaziOd + "\nVazi do: " + lk.vaziDo + "\nPol: " + lk.pol);
            }
        }

        private void btnPrikaziPasos_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika ciji pasos zelite da prikazete!");
                return;
            }

            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}})-[:POSEDUJE_P]->(b:Pasos) return b",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> pasos = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            foreach (Pasos p in pasos)
            {
                MessageBox.Show("Broj pasosa: " + p.brojPasosa + "\nIzdata od: " + p.izdatOd + "\nVazi od: " + p.vaziOd + "\nVazi do: " + p.vaziDo + "\nPol: " + p.pol + "\nDrzavljanstvo: " + p.drzavljanstvo + "\nPrebivaliste: " + p.prebivaliste);
            }
        }

        private void btnPrikaziRegistracije_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika cije registracije vozila zelite da prikazete!");
                return;
            }

            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}})-[:POSEDUJE_RV]->(b:RegistracijaVozila) return b",
                                                                    queryDict, CypherResultMode.Set);

            List<RegistracijaVozila> registracije = ((IRawGraphClient)client).ExecuteGetCypherResults<RegistracijaVozila>(query).ToList();

            foreach (RegistracijaVozila rv in registracije)
            {
                MessageBox.Show("Registarski broj: " + rv.registarskiBroj + "\nVazi do: " + rv.vaziDo + "\nKategorija: " + rv.kategorija);
            }
        }

        private void btnPrikaziKazne_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika cije saobracajne kazne zelite da prikazete!");
                return;
            }

            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}})-[:POSEDUJE_SK]->(b:SaobracajnaKazna) return b",
                                                                    queryDict, CypherResultMode.Set);

            List<SaobracajnaKazna> kazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

            foreach (SaobracajnaKazna sk in kazne)
            {
                MessageBox.Show("Datum kazne: " +sk.datumKazne + "\nRazlog: " + sk.razlog + "\nCena: " + sk.cena);
            }
        }

        private void btnPrikaziVozacku_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika ciju vozacku dozvolu zelite da prikazete!");
                return;
            }

            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}})-[:POSEDUJE_VD]->(b:VozackaDozvola) return b",
                                                                    queryDict, CypherResultMode.Set);

            List<VozackaDozvola> vozacka = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            foreach (VozackaDozvola vd in vozacka)
            {
                MessageBox.Show("Broj vozacke dozvole: " + vd.brojVD + "\nIzdata od: " + vd.izdataOd + "\nVazi od: " + vd.vaziOd + "\nVazi do: " + vd.vaziDo + "\nKategorije: " + vd.kategorije);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormDodajKorisnika DodajKorisnikaForm = new FormDodajKorisnika();
            DodajKorisnikaForm.client = client;
            DodajKorisnikaForm.ShowDialog();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika koga zelite da izmenite!");
                return;
            }

            string korisnikIme = korisnici.SelectedItems[0].SubItems[0].Text;
            string korisnikPrezime = korisnici.SelectedItems[0].SubItems[1].Text;
            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;
            string korisnikDatumRodjenja = korisnici.SelectedItems[0].SubItems[3].Text;
            string korisnikMestoRodjenja = korisnici.SelectedItems[0].SubItems[4].Text;

            FormIzmeniKorisnika IzmeniKorisnikaForm = new FormIzmeniKorisnika(korisnikIme, korisnikPrezime, korisnikJmbg, korisnikDatumRodjenja, korisnikMestoRodjenja);
            IzmeniKorisnikaForm.client = client;
            IzmeniKorisnikaForm.ShowDialog();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (korisnici.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite korisnika koga zelite da obrisete!");
                return;
            }

            string korisnikJmbg = korisnici.SelectedItems[0].SubItems[2].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("korisnikJmbg", korisnikJmbg);

            // PRVO BRISEMO RELACIJE
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}})-[r]-(b) delete r",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            // DRUGO BRISEMO KORISNIKA
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik{jmbg:{korisnikJmbg}}) delete n",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            MessageBox.Show("Korisnik je uspesno obrisan sa svim njegovim relacijama!");
        }
    }
}
