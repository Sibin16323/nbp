﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class IzdaoLK
    {
        public RadnikLicnaKarta radnik { get; set; }
        public LicnaKarta dokument { get; set; }
    }
}
