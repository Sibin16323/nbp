﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class IzdaoRV
    {
        public RadnikRegistracijaVozila radnik { get; set; }
        public RegistracijaVozila dokument { get; set; }
    }
}
