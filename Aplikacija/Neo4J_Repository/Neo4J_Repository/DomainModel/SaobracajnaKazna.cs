﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class SaobracajnaKazna
    {
        public String id { get; set; }
        public String datumKazne { get; set; }
        public String razlog { get; set; }
        public String cena { get; set; }

        public DateTime getDatumKazne()
        {
            if (this.datumKazne == null) return new DateTime();

            long timestamp = Int64.Parse(this.datumKazne);
            DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return startDateTime.AddMilliseconds(timestamp).ToLocalTime();
        }
    }
}
