﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class PosedujeVD
    {
        public Korisnik korisnik { get; set; }
        public VozackaDozvola vozackaDozvola { get; set; }
    }
}
