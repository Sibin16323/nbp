﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class RadnikRegistracijaVozila
    {
        public String id { get; set; }
        public String ime { get; set; }
        public String prezime { get; set; }
        public String jmbg { get; set; }
        public String datumRodjenja { get; set; }
        public String smena { get; set; }

        public DateTime getDatumRodjenja()
        {
            if (this.datumRodjenja == null) return new DateTime();

            long timestamp = Int64.Parse(this.datumRodjenja);
            DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return startDateTime.AddMilliseconds(timestamp).ToLocalTime();
        }

        public List<RegistracijaVozila> listaRegistracijaVozila { get; set; }

        /*public Role playedIn(Movie movie, String role)
        {
            return null;
        }*/
    }
}
