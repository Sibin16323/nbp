﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class IzdaoVD
    {
        public RadnikVozackaDozvola radnik { get; set; }
        public VozackaDozvola dokument { get; set; }
    }
}
