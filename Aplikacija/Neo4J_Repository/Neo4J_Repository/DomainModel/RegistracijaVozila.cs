﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class RegistracijaVozila
    {
        public String id { get; set; }
        public String registarskiBroj { get; set; }
        public String vaziDo { get; set; }
        public String kategorija { get; set; }

        public DateTime getVaziDo()
        {
            if (this.vaziDo == null) return new DateTime();

            long timestamp = Int64.Parse(this.vaziDo);
            DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return startDateTime.AddMilliseconds(timestamp).ToLocalTime();
        }
    }
}
