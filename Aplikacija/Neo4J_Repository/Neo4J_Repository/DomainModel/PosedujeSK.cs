﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class PosedujeSK
    {
        public Korisnik korisnik { get; set; }
        public SaobracajnaKazna saobracajnaKazna { get; set; }
    }
}
