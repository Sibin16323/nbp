﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class Korisnik
    {
        public String id { get; set; }
        public String ime { get; set; }
        public String prezime { get; set; }
        public String jmbg { get; set; }
        public String datumRodjenja { get; set; }
        public String mestoRodjenja { get; set; }

        public DateTime getDatumRodjenja()
        {
            if (this.datumRodjenja == null) return new DateTime();

            long timestamp = Int64.Parse(this.datumRodjenja);
            DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return startDateTime.AddMilliseconds(timestamp).ToLocalTime();
        }

        public LicnaKarta licnaKarta { get; set; }
        public Pasos pasos { get; set; }
        public RegistracijaVozila registracijaVozila { get; set; }
        public VozackaDozvola vozackaDozvola { get; set; }
        public List<SaobracajnaKazna> licnaSaobracajnihKazni { get; set; }

        /*public Role playedIn(Movie movie, String role)
        {
            return null;
        }*/
    }
}
