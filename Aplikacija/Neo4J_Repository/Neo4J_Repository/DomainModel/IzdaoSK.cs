﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class IzdaoSK
    {
        public RadnikSaobracajniPolicajac radnik { get; set; }
        public SaobracajnaKazna dokument { get; set; }
    }
}
