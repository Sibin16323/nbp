﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neo4J_Repository.DomainModel
{
    class LicnaKarta
    {
        public String id { get; set; }
        public String brojLK { get; set; }
        public String izdataOd { get; set; }
        public String vaziOd { get; set; }
        public String vaziDo { get; set; }
        public String pol { get; set; }

        public DateTime getVaziOd()
        {
            if (this.vaziOd == null) return new DateTime();

            long timestamp = Int64.Parse(this.vaziOd);
            DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return startDateTime.AddMilliseconds(timestamp).ToLocalTime();
        }

        public DateTime getVaziDo()
        {
            if (this.vaziDo == null) return new DateTime();

            long timestamp = Int64.Parse(this.vaziDo);
            DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return startDateTime.AddMilliseconds(timestamp).ToLocalTime();
        }
    }
}
