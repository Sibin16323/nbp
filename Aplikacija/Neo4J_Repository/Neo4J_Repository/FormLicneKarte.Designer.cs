﻿namespace Neo4J_Repository
{
    partial class FormLicneKarte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.licneKarte = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnObrisiLicnuKartu = new System.Windows.Forms.Button();
            this.btnDodajLicnuKartu = new System.Windows.Forms.Button();
            this.btnPoPolu = new System.Windows.Forms.Button();
            this.txbPol = new System.Windows.Forms.TextBox();
            this.btnSveLicneKarte = new System.Windows.Forms.Button();
            this.btnPoMestuIzdavanja = new System.Windows.Forms.Button();
            this.btnPoDatumuVazenja = new System.Windows.Forms.Button();
            this.btnPoBrojuLK = new System.Windows.Forms.Button();
            this.txbMestoIzdavanja = new System.Windows.Forms.TextBox();
            this.txbBrojLK = new System.Windows.Forms.TextBox();
            this.txbDatumVazenja = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.licneKarte);
            this.groupBox2.Location = new System.Drawing.Point(13, 13);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(715, 201);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista licnih karata";
            // 
            // licneKarte
            // 
            this.licneKarte.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.licneKarte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.licneKarte.FullRowSelect = true;
            this.licneKarte.GridLines = true;
            this.licneKarte.Location = new System.Drawing.Point(4, 19);
            this.licneKarte.Margin = new System.Windows.Forms.Padding(4);
            this.licneKarte.Name = "licneKarte";
            this.licneKarte.Size = new System.Drawing.Size(707, 178);
            this.licneKarte.TabIndex = 1;
            this.licneKarte.UseCompatibleStateImageBehavior = false;
            this.licneKarte.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Broj licne karte";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Izdata od";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 120;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Vazi od";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vazi do";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Pol";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 100;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnObrisiLicnuKartu);
            this.groupBox1.Controls.Add(this.btnDodajLicnuKartu);
            this.groupBox1.Controls.Add(this.btnPoPolu);
            this.groupBox1.Controls.Add(this.txbPol);
            this.groupBox1.Controls.Add(this.btnSveLicneKarte);
            this.groupBox1.Controls.Add(this.btnPoMestuIzdavanja);
            this.groupBox1.Controls.Add(this.btnPoDatumuVazenja);
            this.groupBox1.Controls.Add(this.btnPoBrojuLK);
            this.groupBox1.Controls.Add(this.txbMestoIzdavanja);
            this.groupBox1.Controls.Add(this.txbBrojLK);
            this.groupBox1.Controls.Add(this.txbDatumVazenja);
            this.groupBox1.Location = new System.Drawing.Point(13, 222);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(715, 201);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // btnObrisiLicnuKartu
            // 
            this.btnObrisiLicnuKartu.Location = new System.Drawing.Point(541, 135);
            this.btnObrisiLicnuKartu.Name = "btnObrisiLicnuKartu";
            this.btnObrisiLicnuKartu.Size = new System.Drawing.Size(167, 50);
            this.btnObrisiLicnuKartu.TabIndex = 10;
            this.btnObrisiLicnuKartu.Text = "Obrisi licnu kartu";
            this.btnObrisiLicnuKartu.UseVisualStyleBackColor = true;
            this.btnObrisiLicnuKartu.Click += new System.EventHandler(this.btnObrisiLicnuKartu_Click);
            // 
            // btnDodajLicnuKartu
            // 
            this.btnDodajLicnuKartu.Location = new System.Drawing.Point(541, 78);
            this.btnDodajLicnuKartu.Name = "btnDodajLicnuKartu";
            this.btnDodajLicnuKartu.Size = new System.Drawing.Size(167, 50);
            this.btnDodajLicnuKartu.TabIndex = 9;
            this.btnDodajLicnuKartu.Text = "Dodaj licnu kartu";
            this.btnDodajLicnuKartu.UseVisualStyleBackColor = true;
            this.btnDodajLicnuKartu.Click += new System.EventHandler(this.btnDodajLicnuKartu_Click);
            // 
            // btnPoPolu
            // 
            this.btnPoPolu.Location = new System.Drawing.Point(174, 151);
            this.btnPoPolu.Name = "btnPoPolu";
            this.btnPoPolu.Size = new System.Drawing.Size(361, 32);
            this.btnPoPolu.TabIndex = 7;
            this.btnPoPolu.Text = "Pretrazi licne karte po polu";
            this.btnPoPolu.UseVisualStyleBackColor = true;
            this.btnPoPolu.Click += new System.EventHandler(this.btnPoPolu_Click);
            // 
            // txbPol
            // 
            this.txbPol.Location = new System.Drawing.Point(7, 156);
            this.txbPol.Name = "txbPol";
            this.txbPol.Size = new System.Drawing.Size(149, 22);
            this.txbPol.TabIndex = 6;
            // 
            // btnSveLicneKarte
            // 
            this.btnSveLicneKarte.Location = new System.Drawing.Point(541, 22);
            this.btnSveLicneKarte.Name = "btnSveLicneKarte";
            this.btnSveLicneKarte.Size = new System.Drawing.Size(167, 50);
            this.btnSveLicneKarte.TabIndex = 8;
            this.btnSveLicneKarte.Text = "Sve licne karte";
            this.btnSveLicneKarte.UseVisualStyleBackColor = true;
            this.btnSveLicneKarte.Click += new System.EventHandler(this.btnSveLicneKarte_Click);
            // 
            // btnPoMestuIzdavanja
            // 
            this.btnPoMestuIzdavanja.Location = new System.Drawing.Point(174, 65);
            this.btnPoMestuIzdavanja.Name = "btnPoMestuIzdavanja";
            this.btnPoMestuIzdavanja.Size = new System.Drawing.Size(361, 32);
            this.btnPoMestuIzdavanja.TabIndex = 3;
            this.btnPoMestuIzdavanja.Text = "Pretrazi licne karte po mestu izdavanja";
            this.btnPoMestuIzdavanja.UseVisualStyleBackColor = true;
            this.btnPoMestuIzdavanja.Click += new System.EventHandler(this.btnPoMestuIzdavanja_Click);
            // 
            // btnPoDatumuVazenja
            // 
            this.btnPoDatumuVazenja.Location = new System.Drawing.Point(174, 110);
            this.btnPoDatumuVazenja.Name = "btnPoDatumuVazenja";
            this.btnPoDatumuVazenja.Size = new System.Drawing.Size(361, 32);
            this.btnPoDatumuVazenja.TabIndex = 5;
            this.btnPoDatumuVazenja.Text = "Pretrazi licne karte po datumu vazenja";
            this.btnPoDatumuVazenja.UseVisualStyleBackColor = true;
            this.btnPoDatumuVazenja.Click += new System.EventHandler(this.btnPoDatumuVazenja_Click);
            // 
            // btnPoBrojuLK
            // 
            this.btnPoBrojuLK.Location = new System.Drawing.Point(174, 22);
            this.btnPoBrojuLK.Name = "btnPoBrojuLK";
            this.btnPoBrojuLK.Size = new System.Drawing.Size(361, 32);
            this.btnPoBrojuLK.TabIndex = 1;
            this.btnPoBrojuLK.Text = "Pretrazi licne karte po broju LK";
            this.btnPoBrojuLK.UseVisualStyleBackColor = true;
            this.btnPoBrojuLK.Click += new System.EventHandler(this.btnPoBrojuLK_Click);
            // 
            // txbMestoIzdavanja
            // 
            this.txbMestoIzdavanja.Location = new System.Drawing.Point(7, 70);
            this.txbMestoIzdavanja.Name = "txbMestoIzdavanja";
            this.txbMestoIzdavanja.Size = new System.Drawing.Size(149, 22);
            this.txbMestoIzdavanja.TabIndex = 2;
            // 
            // txbBrojLK
            // 
            this.txbBrojLK.Location = new System.Drawing.Point(7, 27);
            this.txbBrojLK.Name = "txbBrojLK";
            this.txbBrojLK.Size = new System.Drawing.Size(149, 22);
            this.txbBrojLK.TabIndex = 0;
            // 
            // txbDatumVazenja
            // 
            this.txbDatumVazenja.Location = new System.Drawing.Point(7, 115);
            this.txbDatumVazenja.Name = "txbDatumVazenja";
            this.txbDatumVazenja.Size = new System.Drawing.Size(149, 22);
            this.txbDatumVazenja.TabIndex = 4;
            // 
            // FormLicneKarte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 433);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MaximumSize = new System.Drawing.Size(760, 480);
            this.MinimumSize = new System.Drawing.Size(760, 480);
            this.Name = "FormLicneKarte";
            this.Text = "LICNE KARTE";
            this.Load += new System.EventHandler(this.FormLicneKarte_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView licneKarte;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSveLicneKarte;
        private System.Windows.Forms.Button btnPoPolu;
        private System.Windows.Forms.TextBox txbPol;
        private System.Windows.Forms.Button btnPoMestuIzdavanja;
        private System.Windows.Forms.Button btnPoDatumuVazenja;
        private System.Windows.Forms.Button btnPoBrojuLK;
        private System.Windows.Forms.TextBox txbMestoIzdavanja;
        private System.Windows.Forms.TextBox txbBrojLK;
        private System.Windows.Forms.TextBox txbDatumVazenja;
        private System.Windows.Forms.Button btnDodajLicnuKartu;
        private System.Windows.Forms.Button btnObrisiLicnuKartu;
    }
}