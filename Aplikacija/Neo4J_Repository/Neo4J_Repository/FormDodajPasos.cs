﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajPasos : Form
    {
        public GraphClient client;
        List<Korisnik> sviKorisnici;
        List<RadnikPasos> sviRadnici;
        Pasos dokument;
        public FormDodajPasos()
        {
            InitializeComponent();
        }

        private void FormDodajPasos_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_P]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            foreach (Korisnik k in sviKorisnici)
            {
                this.cbxKorisnici.Items.Add(k.ime + " " + k.prezime + " JBMG: " + k.jmbg);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikPasos) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikPasos>(query2).ToList();

            foreach (RadnikPasos r in sviRadnici)
            {
                this.cbxRadnici.Items.Add(r.ime + " " + r.prezime);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (cbxRadnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati radnika koji ce izdati pasos!");
                return;
            }

            if (cbxKorisnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati korisnika kome ce biti izdat pasos!");
                return;
            }

            if (txbUnosBroj.Text == "")
            {
                MessageBox.Show("Niste uneli broj pasosa!");
                return;
            }

            if (txbUnosBroj.TextLength < 9)
            {
                MessageBox.Show("Broj pasosa mora imati 9 cifara!");
                return;
            }

            if (txbUnosPU.Text == "")
            {
                MessageBox.Show("Niste uneli naziv PU jedinice (Primer: 'PU u Nisu') !");
                return;
            }

            if (rbM.Checked == false && rbZ.Checked == false)
            {
                MessageBox.Show("Niste odabrali pol !");
                return;
            }

            if (txbUnosDrzavljanstvo.Text == "")
            {
                MessageBox.Show("Niste uneli drzavljanstvo (Primer: 'srpsko') !");
                return;
            }

            if (txbUnosPrebivaliste.Text == "")
            {
                MessageBox.Show("Niste uneli prebivaliste (Primer: 'Nis, Srbija') !");
                return;
            }

            Pasos pasos = this.kreirajPasos();
            IzdaoP izdao = this.izdaoPasos();
            PosedujeP poseduje = this.posedujePasos();
            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                pasos.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                pasos.id = "";
            }

            string polM = "M";
            string polZ = "Z";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojPasosa", txbUnosBroj.Text);
            queryDict.Add("izdatOd", txbUnosPU.Text);
            queryDict.Add("vaziOd", dtpDatumOd.Value.ToString());
            queryDict.Add("vaziDo", dtpDatumDo.Value.ToString());
            if (rbM.Checked)
            {
                queryDict.Add("pol", polM);
            }
            else if (rbZ.Checked)
            {
                queryDict.Add("pol", polZ);
            }
            queryDict.Add("drzavljanstvo", txbUnosDrzavljanstvo.Text);
            queryDict.Add("prebivaliste", txbUnosPrebivaliste.Text);

            // SELEKTOVAN KORISNIK
            Korisnik kor = sviKorisnici[cbxKorisnici.SelectedIndex];

            // SELEKTOVAN RADNIK
            RadnikPasos rad = sviRadnici[cbxRadnici.SelectedIndex];

            // KREIRANJE CVORA PASOS
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Pasos {brojPasosa:'" + pasos.brojPasosa
                                                            + "', izdatOd:'" + pasos.izdatOd
                                                            + "', vaziOd:'" + pasos.vaziOd
                                                            + "', vaziDo:'" + pasos.vaziDo
                                                            + "', pol:'" + pasos.pol
                                                            + "', drzavljanstvo:'" + pasos.drzavljanstvo
                                                            + "', prebivaliste:'" + pasos.prebivaliste
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Pasos> pasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            // KREIRANJE RELACIJE KORISNIK -> POSEDUJE
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Korisnik {jmbg:'" + kor.jmbg + "'}), (b:Pasos {brojPasosa:'" + pasos.brojPasosa + "'}) MERGE (a)-[r:POSEDUJE_P]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<PosedujeP> relacije = ((IRawGraphClient)client).ExecuteGetCypherResults<PosedujeP>(query2).ToList();

            // KREIRANJE RELACIJE RADNIK -> IZDAO
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:RadnikPasos {jmbg:'" + rad.jmbg + "'}), (b:Pasos {brojPasosa:'" + pasos.brojPasosa + "'}) MERGE (a)-[r:IZDAO_P]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<IzdaoP> relacije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<IzdaoP>(query3).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (Pasos p in pasosi)
            {
                MessageBox.Show("Pasos sa brojem: " + p.brojPasosa + " je uspesno kreiran za korisnika " + kor.ime + " " + kor.prezime + "!");
            }

            this.Close();
        }

        private Pasos kreirajPasos()
        {
            dokument = new Pasos();

            dokument.brojPasosa = txbUnosBroj.Text;
            dokument.izdatOd = txbUnosPU.Text;
            dokument.vaziOd = dtpDatumOd.Value.Date.ToString("dd.MM.yyyy.");
            dokument.vaziDo = dtpDatumDo.Value.Date.ToString("dd.MM.yyyy.");
            if (rbM.Checked == true)
                dokument.pol = "M";
            else
                dokument.pol = "Z";
            dokument.drzavljanstvo = txbUnosDrzavljanstvo.Text;
            dokument.prebivaliste = txbUnosPrebivaliste.Text;

            return dokument;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private IzdaoP izdaoPasos()
        {
            IzdaoP p = new IzdaoP();

            RadnikPasos radnik2 = sviRadnici[cbxRadnici.SelectedIndex];

            p.radnik = radnik2;
            p.dokument = this.dokument;

            return p;
        }

        private PosedujeP posedujePasos()
        {
            PosedujeP p = new PosedujeP();

            Korisnik korisnik2 = sviKorisnici[cbxKorisnici.SelectedIndex];

            p.korisnik = korisnik2;
            p.pasos = this.dokument;

            return p;
        }
    }
}
