﻿namespace Neo4J_Repository
{
    partial class FormSaobracajneKazne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPoCeni = new System.Windows.Forms.Button();
            this.txbCena = new System.Windows.Forms.TextBox();
            this.btnSveSaobracajneKazne = new System.Windows.Forms.Button();
            this.btnPoRazlogu = new System.Windows.Forms.Button();
            this.btnPoDatumu = new System.Windows.Forms.Button();
            this.txbDatum = new System.Windows.Forms.TextBox();
            this.txbRazlog = new System.Windows.Forms.TextBox();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saobracajneKazne = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPoCeni
            // 
            this.btnPoCeni.Location = new System.Drawing.Point(174, 110);
            this.btnPoCeni.Name = "btnPoCeni";
            this.btnPoCeni.Size = new System.Drawing.Size(281, 32);
            this.btnPoCeni.TabIndex = 5;
            this.btnPoCeni.Text = "Pretrazi kazne po ceni";
            this.btnPoCeni.UseVisualStyleBackColor = true;
            this.btnPoCeni.Click += new System.EventHandler(this.btnPoCeni_Click);
            // 
            // txbCena
            // 
            this.txbCena.Location = new System.Drawing.Point(7, 115);
            this.txbCena.Name = "txbCena";
            this.txbCena.Size = new System.Drawing.Size(149, 22);
            this.txbCena.TabIndex = 4;
            // 
            // btnSveSaobracajneKazne
            // 
            this.btnSveSaobracajneKazne.Location = new System.Drawing.Point(461, 22);
            this.btnSveSaobracajneKazne.Name = "btnSveSaobracajneKazne";
            this.btnSveSaobracajneKazne.Size = new System.Drawing.Size(192, 33);
            this.btnSveSaobracajneKazne.TabIndex = 6;
            this.btnSveSaobracajneKazne.Text = "Sve saobracajne kazne";
            this.btnSveSaobracajneKazne.UseVisualStyleBackColor = true;
            this.btnSveSaobracajneKazne.Click += new System.EventHandler(this.btnSveSaobracajneKazne_Click);
            // 
            // btnPoRazlogu
            // 
            this.btnPoRazlogu.Location = new System.Drawing.Point(174, 66);
            this.btnPoRazlogu.Name = "btnPoRazlogu";
            this.btnPoRazlogu.Size = new System.Drawing.Size(281, 33);
            this.btnPoRazlogu.TabIndex = 3;
            this.btnPoRazlogu.Text = "Pretrazi kazne po razlogu";
            this.btnPoRazlogu.UseVisualStyleBackColor = true;
            this.btnPoRazlogu.Click += new System.EventHandler(this.btnPoRazlogu_Click);
            // 
            // btnPoDatumu
            // 
            this.btnPoDatumu.Location = new System.Drawing.Point(174, 22);
            this.btnPoDatumu.Name = "btnPoDatumu";
            this.btnPoDatumu.Size = new System.Drawing.Size(281, 33);
            this.btnPoDatumu.TabIndex = 1;
            this.btnPoDatumu.Text = "Pretrazi kazne po datumu";
            this.btnPoDatumu.UseVisualStyleBackColor = true;
            this.btnPoDatumu.Click += new System.EventHandler(this.btnPoDatumu_Click);
            // 
            // txbDatum
            // 
            this.txbDatum.Location = new System.Drawing.Point(7, 27);
            this.txbDatum.Name = "txbDatum";
            this.txbDatum.Size = new System.Drawing.Size(149, 22);
            this.txbDatum.TabIndex = 0;
            // 
            // txbRazlog
            // 
            this.txbRazlog.Location = new System.Drawing.Point(7, 71);
            this.txbRazlog.Name = "txbRazlog";
            this.txbRazlog.Size = new System.Drawing.Size(149, 22);
            this.txbRazlog.TabIndex = 2;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cena";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Razlog";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 180;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Datum kazne";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 120;
            // 
            // saobracajneKazne
            // 
            this.saobracajneKazne.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.saobracajneKazne.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saobracajneKazne.FullRowSelect = true;
            this.saobracajneKazne.GridLines = true;
            this.saobracajneKazne.Location = new System.Drawing.Point(4, 19);
            this.saobracajneKazne.Margin = new System.Windows.Forms.Padding(4);
            this.saobracajneKazne.Name = "saobracajneKazne";
            this.saobracajneKazne.Size = new System.Drawing.Size(652, 178);
            this.saobracajneKazne.TabIndex = 1;
            this.saobracajneKazne.UseCompatibleStateImageBehavior = false;
            this.saobracajneKazne.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnObrisi);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.btnPoCeni);
            this.groupBox1.Controls.Add(this.txbCena);
            this.groupBox1.Controls.Add(this.btnSveSaobracajneKazne);
            this.groupBox1.Controls.Add(this.btnPoRazlogu);
            this.groupBox1.Controls.Add(this.btnPoDatumu);
            this.groupBox1.Controls.Add(this.txbDatum);
            this.groupBox1.Controls.Add(this.txbRazlog);
            this.groupBox1.Location = new System.Drawing.Point(13, 222);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(660, 152);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(461, 109);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(192, 33);
            this.btnObrisi.TabIndex = 8;
            this.btnObrisi.Text = "Obrisi saobracajnu kaznu";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(461, 66);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(192, 33);
            this.btnDodaj.TabIndex = 7;
            this.btnDodaj.Text = "Dodaj saobracajnu kaznu";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.saobracajneKazne);
            this.groupBox2.Location = new System.Drawing.Point(13, 13);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(660, 201);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista saobracajnih kazni";
            // 
            // FormSaobracajneKazne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 383);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MaximumSize = new System.Drawing.Size(700, 430);
            this.MinimumSize = new System.Drawing.Size(700, 430);
            this.Name = "FormSaobracajneKazne";
            this.Text = "SAOBRACAJNE KAZNE";
            this.Load += new System.EventHandler(this.FormSaobracajneKazne_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPoCeni;
        private System.Windows.Forms.TextBox txbCena;
        private System.Windows.Forms.Button btnSveSaobracajneKazne;
        private System.Windows.Forms.Button btnPoRazlogu;
        private System.Windows.Forms.Button btnPoDatumu;
        private System.Windows.Forms.TextBox txbDatum;
        private System.Windows.Forms.TextBox txbRazlog;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView saobracajneKazne;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnObrisi;
    }
}