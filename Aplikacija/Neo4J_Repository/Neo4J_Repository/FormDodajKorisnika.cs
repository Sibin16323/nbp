﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajKorisnika : Form
    {
        public GraphClient client;
        public FormDodajKorisnika()
        {
            InitializeComponent();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (txbUnosIme.Text == "")
            {
                MessageBox.Show("Niste uneli ime korisnika!");
                return;
            }

            if (txbUnosPrezime.Text == "")
            {
                MessageBox.Show("Niste uneli prezime korisnika!");
                return;
            }

            if (txbUnosJMBG.Text == "")
            {
                MessageBox.Show("Niste uneli JMBG korisnika!");
                return;
            }

            if (txbUnosJMBG.TextLength < 13)
            {
                MessageBox.Show("JMBG mora imati 13 cifara!");
                return;
            }

            if (txbUnosMestoRodjenja.Text == "")
            {
                MessageBox.Show("Niste uneli mesto rodjenja korisnika!");
                return;
            }

            Korisnik kor = this.kreirajKorisnika();
            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                kor.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                kor.id = "";
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("ime", txbUnosIme.Text);
            queryDict.Add("prezime", txbUnosPrezime.Text);
            queryDict.Add("jmbg", txbUnosJMBG.Text);
            queryDict.Add("datumRodjenja", dtpDatumRodjenja.Value.ToString());
            queryDict.Add("mestoRodjenja", txbUnosMestoRodjenja.Text);

            // KREIRANJE CVORA KORISNIK
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Korisnik {ime:'" + kor.ime
                                                            + "', prezime:'" + kor.prezime
                                                            + "', jmbg:'" + kor.jmbg
                                                            + "', datumRodjenja:'" + kor.datumRodjenja
                                                            + "', mestoRodjenja:'" + kor.mestoRodjenja
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Korisnik> korisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (Korisnik k in korisnici)
            {
                MessageBox.Show("Korisnik: " + k.ime + " " + k.prezime + " je uspesno kreiran!");
            }

            this.Close();
        }

        private Korisnik kreirajKorisnika()
        {
            Korisnik korisnik = new Korisnik();

            korisnik.ime = txbUnosIme.Text;
            korisnik.prezime = txbUnosPrezime.Text;
            korisnik.jmbg = txbUnosJMBG.Text;
            korisnik.datumRodjenja = dtpDatumRodjenja.Value.Date.ToString("dd.MM.yyyy.");
            korisnik.mestoRodjenja = txbUnosMestoRodjenja.Text;

            return korisnik;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }
    }
}
