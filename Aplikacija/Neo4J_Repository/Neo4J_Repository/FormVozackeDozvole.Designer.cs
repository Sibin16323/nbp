﻿namespace Neo4J_Repository
{
    partial class FormVozackeDozvole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.vozackeDozvole = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnPoKategorijama = new System.Windows.Forms.Button();
            this.txbDatumVazenja = new System.Windows.Forms.TextBox();
            this.btnSveVozackeDozvole = new System.Windows.Forms.Button();
            this.btnPoMestuIzdavanja = new System.Windows.Forms.Button();
            this.btnPoDatumuVazenja = new System.Windows.Forms.Button();
            this.btnPoBrojuVozacke = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.txbKategorije = new System.Windows.Forms.TextBox();
            this.txbMestoIzdavanja = new System.Windows.Forms.TextBox();
            this.txbBrojVozacke = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.vozackeDozvole);
            this.groupBox2.Location = new System.Drawing.Point(14, 12);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(713, 201);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista vozackih dozvola";
            // 
            // vozackeDozvole
            // 
            this.vozackeDozvole.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.vozackeDozvole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vozackeDozvole.FullRowSelect = true;
            this.vozackeDozvole.GridLines = true;
            this.vozackeDozvole.Location = new System.Drawing.Point(4, 19);
            this.vozackeDozvole.Margin = new System.Windows.Forms.Padding(4);
            this.vozackeDozvole.Name = "vozackeDozvole";
            this.vozackeDozvole.Size = new System.Drawing.Size(705, 178);
            this.vozackeDozvole.TabIndex = 1;
            this.vozackeDozvole.UseCompatibleStateImageBehavior = false;
            this.vozackeDozvole.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Broj vozacke dozvole";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 125;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Izdata od";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Vazi od";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vazi do";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Kategorije";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 100;
            // 
            // btnPoKategorijama
            // 
            this.btnPoKategorijama.Location = new System.Drawing.Point(174, 151);
            this.btnPoKategorijama.Name = "btnPoKategorijama";
            this.btnPoKategorijama.Size = new System.Drawing.Size(353, 32);
            this.btnPoKategorijama.TabIndex = 7;
            this.btnPoKategorijama.Text = "Pretrazi vozacke po kategorijama";
            this.btnPoKategorijama.UseVisualStyleBackColor = true;
            this.btnPoKategorijama.Click += new System.EventHandler(this.btnPoKategorijama_Click);
            // 
            // txbDatumVazenja
            // 
            this.txbDatumVazenja.Location = new System.Drawing.Point(7, 115);
            this.txbDatumVazenja.Name = "txbDatumVazenja";
            this.txbDatumVazenja.Size = new System.Drawing.Size(149, 22);
            this.txbDatumVazenja.TabIndex = 4;
            // 
            // btnSveVozackeDozvole
            // 
            this.btnSveVozackeDozvole.Location = new System.Drawing.Point(533, 22);
            this.btnSveVozackeDozvole.Name = "btnSveVozackeDozvole";
            this.btnSveVozackeDozvole.Size = new System.Drawing.Size(167, 50);
            this.btnSveVozackeDozvole.TabIndex = 8;
            this.btnSveVozackeDozvole.Text = "Sve vozacke dozvole";
            this.btnSveVozackeDozvole.UseVisualStyleBackColor = true;
            this.btnSveVozackeDozvole.Click += new System.EventHandler(this.btnSveVozackeDozvole_Click);
            // 
            // btnPoMestuIzdavanja
            // 
            this.btnPoMestuIzdavanja.Location = new System.Drawing.Point(174, 65);
            this.btnPoMestuIzdavanja.Name = "btnPoMestuIzdavanja";
            this.btnPoMestuIzdavanja.Size = new System.Drawing.Size(353, 32);
            this.btnPoMestuIzdavanja.TabIndex = 3;
            this.btnPoMestuIzdavanja.Text = "Pretrazi vozacke po mestu izdavanja";
            this.btnPoMestuIzdavanja.UseVisualStyleBackColor = true;
            this.btnPoMestuIzdavanja.Click += new System.EventHandler(this.btnPoMestuIzdavanja_Click);
            // 
            // btnPoDatumuVazenja
            // 
            this.btnPoDatumuVazenja.Location = new System.Drawing.Point(174, 110);
            this.btnPoDatumuVazenja.Name = "btnPoDatumuVazenja";
            this.btnPoDatumuVazenja.Size = new System.Drawing.Size(353, 32);
            this.btnPoDatumuVazenja.TabIndex = 5;
            this.btnPoDatumuVazenja.Text = "Pretrazi vozacke po datumu vazenja";
            this.btnPoDatumuVazenja.UseVisualStyleBackColor = true;
            this.btnPoDatumuVazenja.Click += new System.EventHandler(this.btnPoDatumuVazenja_Click);
            // 
            // btnPoBrojuVozacke
            // 
            this.btnPoBrojuVozacke.Location = new System.Drawing.Point(174, 22);
            this.btnPoBrojuVozacke.Name = "btnPoBrojuVozacke";
            this.btnPoBrojuVozacke.Size = new System.Drawing.Size(353, 32);
            this.btnPoBrojuVozacke.TabIndex = 1;
            this.btnPoBrojuVozacke.Text = "Pretrazi vozacke po broju vozacke";
            this.btnPoBrojuVozacke.UseVisualStyleBackColor = true;
            this.btnPoBrojuVozacke.Click += new System.EventHandler(this.btnPoBrojuVozacke_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnObrisi);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.btnPoKategorijama);
            this.groupBox1.Controls.Add(this.txbKategorije);
            this.groupBox1.Controls.Add(this.btnSveVozackeDozvole);
            this.groupBox1.Controls.Add(this.btnPoMestuIzdavanja);
            this.groupBox1.Controls.Add(this.btnPoDatumuVazenja);
            this.groupBox1.Controls.Add(this.btnPoBrojuVozacke);
            this.groupBox1.Controls.Add(this.txbMestoIzdavanja);
            this.groupBox1.Controls.Add(this.txbBrojVozacke);
            this.groupBox1.Controls.Add(this.txbDatumVazenja);
            this.groupBox1.Location = new System.Drawing.Point(14, 221);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(713, 193);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(533, 133);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(167, 50);
            this.btnObrisi.TabIndex = 10;
            this.btnObrisi.Text = "Obrisi vozacku dozvolu";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(533, 78);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(167, 50);
            this.btnDodaj.TabIndex = 9;
            this.btnDodaj.Text = "Dodaj vozacku dozvolu";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // txbKategorije
            // 
            this.txbKategorije.Location = new System.Drawing.Point(7, 156);
            this.txbKategorije.Name = "txbKategorije";
            this.txbKategorije.Size = new System.Drawing.Size(149, 22);
            this.txbKategorije.TabIndex = 6;
            // 
            // txbMestoIzdavanja
            // 
            this.txbMestoIzdavanja.Location = new System.Drawing.Point(7, 70);
            this.txbMestoIzdavanja.Name = "txbMestoIzdavanja";
            this.txbMestoIzdavanja.Size = new System.Drawing.Size(149, 22);
            this.txbMestoIzdavanja.TabIndex = 2;
            // 
            // txbBrojVozacke
            // 
            this.txbBrojVozacke.Location = new System.Drawing.Point(7, 27);
            this.txbBrojVozacke.Name = "txbBrojVozacke";
            this.txbBrojVozacke.Size = new System.Drawing.Size(149, 22);
            this.txbBrojVozacke.TabIndex = 0;
            // 
            // FormVozackeDozvole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 423);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(755, 470);
            this.MinimumSize = new System.Drawing.Size(755, 470);
            this.Name = "FormVozackeDozvole";
            this.Text = "VOZACKE DOZVOLE";
            this.Load += new System.EventHandler(this.FormVozackeDozvole_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView vozackeDozvole;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnPoKategorijama;
        private System.Windows.Forms.TextBox txbDatumVazenja;
        private System.Windows.Forms.Button btnSveVozackeDozvole;
        private System.Windows.Forms.Button btnPoMestuIzdavanja;
        private System.Windows.Forms.Button btnPoDatumuVazenja;
        private System.Windows.Forms.Button btnPoBrojuVozacke;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txbKategorije;
        private System.Windows.Forms.TextBox txbMestoIzdavanja;
        private System.Windows.Forms.TextBox txbBrojVozacke;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnObrisi;
    }
}