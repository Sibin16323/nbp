﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormVozackeDozvole : Form
    {
        public GraphClient client;
        public FormVozackeDozvole()
        {
            InitializeComponent();
        }

        private void btnPoBrojuVozacke_Click(object sender, EventArgs e)
        {
            if (txbBrojVozacke.Text == "")
            {
                MessageBox.Show("Niste uneli broj vozacke dozvole!");
                return;
            }

            string brojVozacke = ".*" + txbBrojVozacke.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojVozacke", brojVozacke);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:VozackaDozvola) and exists(n.brojVD) and n.brojVD =~ {brojVozacke} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<VozackaDozvola> sveVozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            this.vozackeDozvole.Items.Clear();

            foreach (VozackaDozvola vd in sveVozacke)
            {
                ListViewItem item = new ListViewItem(new string[] { vd.brojVD, vd.izdataOd, vd.vaziOd, vd.vaziDo, vd.kategorije });
                this.vozackeDozvole.Items.Add(item);
            }

            this.vozackeDozvole.Refresh();
        }

        private void btnPoMestuIzdavanja_Click(object sender, EventArgs e)
        {
            if (txbMestoIzdavanja.Text == "")
            {
                MessageBox.Show("Niste uneli mesto izdavanja vozacke dozvole!");
                return;
            }

            string mestoIzdavanja = ".*" + txbMestoIzdavanja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("mestoIzdavanja", mestoIzdavanja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:VozackaDozvola) and exists(n.izdataOd) and n.izdataOd =~ {mestoIzdavanja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<VozackaDozvola> sveVozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            this.vozackeDozvole.Items.Clear();

            foreach (VozackaDozvola vd in sveVozacke)
            {
                ListViewItem item = new ListViewItem(new string[] { vd.brojVD, vd.izdataOd, vd.vaziOd, vd.vaziDo, vd.kategorije });
                this.vozackeDozvole.Items.Add(item);
            }

            this.vozackeDozvole.Refresh();
        }

        private void btnPoDatumuVazenja_Click(object sender, EventArgs e)
        {
            if (txbDatumVazenja.Text == "")
            {
                MessageBox.Show("Niste uneli datum vazenja vozacke dozvole!");
                return;
            }

            string datumVazenja = ".*" + txbDatumVazenja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datumVazenja", datumVazenja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:VozackaDozvola) and exists(n.vaziDo) and n.vaziDo =~ {datumVazenja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<VozackaDozvola> sveVozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            this.vozackeDozvole.Items.Clear();

            foreach (VozackaDozvola vd in sveVozacke)
            {
                ListViewItem item = new ListViewItem(new string[] { vd.brojVD, vd.izdataOd, vd.vaziOd, vd.vaziDo, vd.kategorije });
                this.vozackeDozvole.Items.Add(item);
            }

            this.vozackeDozvole.Refresh();
        }

        private void btnPoKategorijama_Click(object sender, EventArgs e)
        {
            if (txbKategorije.Text == "")
            {
                MessageBox.Show("Niste uneli kategorije vozacke dozvole!");
                return;
            }

            string kategorije = ".*" + txbKategorije.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("kategorije", kategorije);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:VozackaDozvola) and exists(n.kategorije) and n.kategorije =~ {kategorije} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<VozackaDozvola> sveVozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            this.vozackeDozvole.Items.Clear();

            foreach (VozackaDozvola vd in sveVozacke)
            {
                ListViewItem item = new ListViewItem(new string[] { vd.brojVD, vd.izdataOd, vd.vaziOd, vd.vaziDo, vd.kategorije });
                this.vozackeDozvole.Items.Add(item);
            }

            this.vozackeDozvole.Refresh();
        }

        private void btnSveVozackeDozvole_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:VozackaDozvola) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<VozackaDozvola> sveVozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            this.vozackeDozvole.Items.Clear();

            foreach (VozackaDozvola vd in sveVozacke)
            {
                ListViewItem item = new ListViewItem(new string[] { vd.brojVD, vd.izdataOd, vd.vaziOd, vd.vaziDo, vd.kategorije });
                this.vozackeDozvole.Items.Add(item);
            }

            this.vozackeDozvole.Refresh();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormDodajVozackuDozvolu DodajVozackuDozvoluForm = new FormDodajVozackuDozvolu();
            DodajVozackuDozvoluForm.client = client;
            DodajVozackuDozvoluForm.ShowDialog();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (vozackeDozvole.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite vozacku dozvolu koju zelite da obrisete!");
                return;
            }

            string brojVozackeDozvole = vozackeDozvole.SelectedItems[0].SubItems[0].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojVozackeDozvole", brojVozackeDozvole);

            // PRVO BRISEMO RELACIJE
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (a)-[r]-(b:VozackaDozvola{brojVD:{brojVozackeDozvole}}) delete r",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            // DRUGO BRISEMO VOZACKU DOZVOLU
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:VozackaDozvola{brojVD:{brojVozackeDozvole}}) delete n",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            MessageBox.Show("Vozacka dozvola je uspesno obrisana sa svim njenim relacijama!");
        }

        private void FormVozackeDozvole_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:VozackaDozvola) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<VozackaDozvola> sveVozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            this.vozackeDozvole.Items.Clear();

            foreach (VozackaDozvola vd in sveVozacke)
            {
                ListViewItem item = new ListViewItem(new string[] { vd.brojVD, vd.izdataOd, vd.vaziOd, vd.vaziDo, vd.kategorije });
                this.vozackeDozvole.Items.Add(item);
            }

            this.vozackeDozvole.Refresh();
        }
    }
}
