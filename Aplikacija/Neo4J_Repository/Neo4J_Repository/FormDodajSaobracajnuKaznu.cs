﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajSaobracajnuKaznu : Form
    {
        public GraphClient client;
        List<Korisnik> sviKorisnici;
        List<RadnikSaobracajniPolicajac> sviRadnici;
        SaobracajnaKazna dokument;
        public FormDodajSaobracajnuKaznu()
        {
            InitializeComponent();
        }

        private void FormDodajSaobracajnuKaznu_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            foreach (Korisnik k in sviKorisnici)
            {
                this.cbxKorisnici.Items.Add(k.ime + " " + k.prezime + " JBMG: " + k.jmbg);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikSaobracajniPolicajac) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikSaobracajniPolicajac>(query2).ToList();

            foreach (RadnikSaobracajniPolicajac r in sviRadnici)
            {
                this.cbxRadnici.Items.Add(r.ime + " " + r.prezime);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (cbxRadnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati radnika koji ce izdati saobracajnu kaznu!");
                return;
            }

            if (cbxKorisnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati korisnika kome ce biti izdata saobracajna kazna!");
                return;
            }

            if (txbUnosRazlog.Text == "")
            {
                MessageBox.Show("Niste uneli razlog saobracajne kazne (Primer: 'Sigurnosni pojas') !");
                return;
            }

            if (txbUnosCena.Text == "")
            {
                MessageBox.Show("Niste uneli cenu saobracajne kazne (Primer: '5000 RSD') !");
                return;
            }

            SaobracajnaKazna kazna = this.kreirajKaznu();
            IzdaoSK izdao = this.izdaoKaznu();
            PosedujeSK poseduje = this.posedujeKaznu();
            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                kazna.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                kazna.id = "";
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datumKazne", dtpDatumOd.Value.ToString());
            queryDict.Add("razlog", txbUnosRazlog.Text);
            queryDict.Add("cena", txbUnosCena.Text);

            // SELEKTOVAN KORISNIK
            Korisnik kor = sviKorisnici[cbxKorisnici.SelectedIndex];

            // SELEKTOVAN RADNIK
            RadnikSaobracajniPolicajac rad = sviRadnici[cbxRadnici.SelectedIndex];
            
            // KREIRANJE CVORA SAOBRACAJNAKAZNA
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:SaobracajnaKazna {datumKazne:'" + kazna.datumKazne
                                                            + "', razlog:'" + kazna.razlog
                                                            + "', cena:'" + kazna.cena
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<SaobracajnaKazna> kazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

            // KREIRANJE RELACIJE KORISNIK -> POSEDUJE
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Korisnik {jmbg:'" + kor.jmbg + "'}), (b:SaobracajnaKazna {datumKazne:'" + kazna.datumKazne + "'}) MERGE (a)-[r:POSEDUJE_SK]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<PosedujeSK> relacije = ((IRawGraphClient)client).ExecuteGetCypherResults<PosedujeSK>(query2).ToList();

            // KREIRANJE RELACIJE RADNIK -> IZDAO
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:RadnikSaobracajniPolicajac {jmbg:'" + rad.jmbg + "'}), (b:SaobracajnaKazna {datumKazne:'" + kazna.datumKazne + "'}) MERGE (a)-[r:IZDAO_SK]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<IzdaoSK> relacije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<IzdaoSK>(query3).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (SaobracajnaKazna s in kazne)
            {
                MessageBox.Show("Saobracajna kazna sa razlogom: " + s.razlog + " je uspesno kreirana za korisnika " + kor.ime + " " + kor.prezime + "!");
            }

            this.Close();
        }

        private SaobracajnaKazna kreirajKaznu()
        {
            dokument = new SaobracajnaKazna();
            
            dokument.datumKazne = dtpDatumOd.Value.Date.ToString("dd.MM.yyyy.");
            dokument.razlog = txbUnosRazlog.Text;
            dokument.cena = txbUnosCena.Text;

            return dokument;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private IzdaoSK izdaoKaznu()
        {
            IzdaoSK s = new IzdaoSK();

            RadnikSaobracajniPolicajac radnik2 = sviRadnici[cbxRadnici.SelectedIndex];

            s.radnik = radnik2;
            s.dokument = this.dokument;

            return s;
        }

        private PosedujeSK posedujeKaznu()
        {
            PosedujeSK s = new PosedujeSK();

            Korisnik korisnik2 = sviKorisnici[cbxKorisnici.SelectedIndex];

            s.korisnik = korisnik2;
            s.saobracajnaKazna = this.dokument;

            return s;
        }
    }
}
