﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormDodajVozackuDozvolu : Form
    {
        public GraphClient client;
        List<Korisnik> sviKorisnici;
        List<RadnikVozackaDozvola> sviRadnici;
        VozackaDozvola dokument;
        public FormDodajVozackuDozvolu()
        {
            InitializeComponent();
        }

        private void FormDodajVozackuDozvolu_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Korisnik) WHERE NOT (n)-[:POSEDUJE_VD]->() return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviKorisnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Korisnik>(query).ToList();

            foreach (Korisnik k in sviKorisnici)
            {
                this.cbxKorisnici.Items.Add(k.ime + " " + k.prezime + " JBMG: " + k.jmbg);
            }

            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:RadnikVozackaDozvola) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            sviRadnici = ((IRawGraphClient)client).ExecuteGetCypherResults<RadnikVozackaDozvola>(query2).ToList();

            foreach (RadnikVozackaDozvola r in sviRadnici)
            {
                this.cbxRadnici.Items.Add(r.ime + " " + r.prezime);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (cbxRadnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati radnika koji ce izdati vozacku dozvolu!");
                return;
            }

            if (cbxKorisnici.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati korisnika kome ce biti izdata vozacka dozvola!");
                return;
            }

            if (txbUnosBroj.Text == "")
            {
                MessageBox.Show("Niste uneli broj vozacke dozvole!");
                return;
            }

            if (txbUnosBroj.TextLength < 9)
            {
                MessageBox.Show("Broj vozacke dozvole mora imati 9 cifara!");
                return;
            }

            if (txbUnosPU.Text == "")
            {
                MessageBox.Show("Niste uneli naziv PU jedinice (Primer: 'PU u Nisu') !");
                return;
            }

            if (txbUnosKategorije.Text == "")
            {
                MessageBox.Show("Niste uneli kategorije (Primer: 'A,B,C') !");
                return;
            }

            VozackaDozvola vozacka = this.kreirajVozacku();
            IzdaoVD izdao = this.izdaoVozacku();
            PosedujeVD poseduje = this.posedujeVozacku();
            string maxId = getMaxId();

            try
            {
                int mId = Int32.Parse(maxId);
                vozacka.id = (mId++).ToString();
            }
            catch (Exception exception)
            {
                vozacka.id = "";
            }

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojVD", txbUnosBroj.Text);
            queryDict.Add("izdataOd", txbUnosPU.Text);
            queryDict.Add("vaziOd", dtpDatumOd.Value.ToString());
            queryDict.Add("vaziDo", dtpDatumDo.Value.ToString());
            queryDict.Add("kategorije", txbUnosKategorije.Text);

            // SELEKTOVAN KORISNIK
            Korisnik kor = sviKorisnici[cbxKorisnici.SelectedIndex];

            // SELEKTOVAN RADNIK
            RadnikVozackaDozvola rad = sviRadnici[cbxRadnici.SelectedIndex];

            // KREIRANJE CVORA VOZACKADOZVOLA
            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:VozackaDozvola {brojVD:'" + vozacka.brojVD
                                                            + "', izdataOd:'" + vozacka.izdataOd
                                                            + "', vaziOd:'" + vozacka.vaziOd
                                                            + "', vaziDo:'" + vozacka.vaziDo
                                                            + "', kategorije:'" + vozacka.kategorije
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<VozackaDozvola> vozacke = ((IRawGraphClient)client).ExecuteGetCypherResults<VozackaDozvola>(query).ToList();

            // KREIRANJE RELACIJE KORISNIK -> POSEDUJE
            var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:Korisnik {jmbg:'" + kor.jmbg + "'}), (b:VozackaDozvola {brojVD:'" + vozacka.brojVD + "'}) MERGE (a)-[r:POSEDUJE_VD]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<PosedujeVD> relacije = ((IRawGraphClient)client).ExecuteGetCypherResults<PosedujeVD>(query2).ToList();

            // KREIRANJE RELACIJE RADNIK -> IZDAO
            var query3 = new Neo4jClient.Cypher.CypherQuery("MATCH (a:RadnikVozackaDozvola {jmbg:'" + rad.jmbg + "'}), (b:VozackaDozvola {brojVD:'" + vozacka.brojVD + "'}) MERGE (a)-[r:IZDAO_VD]->(b) return r",
                                                            queryDict, CypherResultMode.Set);

            List<IzdaoVD> relacije2 = ((IRawGraphClient)client).ExecuteGetCypherResults<IzdaoVD>(query3).ToList();

            // ISPIS KREIRANOG REZULTATA
            foreach (VozackaDozvola v in vozacke)
            {
                MessageBox.Show("Vozacka dozvola sa brojem: " + v.brojVD + " je uspesno kreirana za korisnika " + kor.ime + " " + kor.prezime + "!");
            }

            this.Close();
        }

        private VozackaDozvola kreirajVozacku()
        {
            dokument = new VozackaDozvola();

            dokument.brojVD = txbUnosBroj.Text;
            dokument.izdataOd = txbUnosPU.Text;
            dokument.vaziOd = dtpDatumOd.Value.Date.ToString("dd.MM.yyyy.");
            dokument.vaziDo = dtpDatumDo.Value.Date.ToString("dd.MM.yyyy.");
            dokument.kategorije = txbUnosKategorije.Text;

            return dokument;
        }

        private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }

        private IzdaoVD izdaoVozacku()
        {
            IzdaoVD p = new IzdaoVD();

            RadnikVozackaDozvola radnik2 = sviRadnici[cbxRadnici.SelectedIndex];

            p.radnik = radnik2;
            p.dokument = this.dokument;

            return p;
        }

        private PosedujeVD posedujeVozacku()
        {
            PosedujeVD v = new PosedujeVD();

            Korisnik korisnik2 = sviKorisnici[cbxKorisnici.SelectedIndex];

            v.korisnik = korisnik2;
            v.vozackaDozvola = this.dokument;

            return v;
        }
    }
}
