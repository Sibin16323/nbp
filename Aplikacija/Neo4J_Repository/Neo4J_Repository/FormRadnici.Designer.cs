﻿namespace Neo4J_Repository
{
    partial class FormRadnici
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrikaziVozacke = new System.Windows.Forms.Button();
            this.btnPrikaziKazne = new System.Windows.Forms.Button();
            this.btnPrikaziRegistracije = new System.Windows.Forms.Button();
            this.btnPrikaziPasose = new System.Windows.Forms.Button();
            this.btnPrikaziLicneKarte = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnPoDatumuRodjenja = new System.Windows.Forms.Button();
            this.btnPoImenu = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnPoSmeni = new System.Windows.Forms.Button();
            this.txbSmena = new System.Windows.Forms.TextBox();
            this.btnPoJMBG = new System.Windows.Forms.Button();
            this.txbDatumRodjenja = new System.Windows.Forms.TextBox();
            this.txbIme = new System.Windows.Forms.TextBox();
            this.txbJmbg = new System.Windows.Forms.TextBox();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.radnici = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSaP = new System.Windows.Forms.Button();
            this.btnSaRV = new System.Windows.Forms.Button();
            this.btnSaSK = new System.Windows.Forms.Button();
            this.btnSaVD = new System.Windows.Forms.Button();
            this.btnSaLK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrikaziVozacke
            // 
            this.btnPrikaziVozacke.Location = new System.Drawing.Point(835, 191);
            this.btnPrikaziVozacke.Name = "btnPrikaziVozacke";
            this.btnPrikaziVozacke.Size = new System.Drawing.Size(150, 81);
            this.btnPrikaziVozacke.TabIndex = 12;
            this.btnPrikaziVozacke.Text = "Prikazi VOZACKE DOZVOLE koje je radnik dodelio";
            this.btnPrikaziVozacke.UseVisualStyleBackColor = true;
            this.btnPrikaziVozacke.Click += new System.EventHandler(this.btnPrikaziVozacke_Click);
            // 
            // btnPrikaziKazne
            // 
            this.btnPrikaziKazne.Location = new System.Drawing.Point(636, 191);
            this.btnPrikaziKazne.Name = "btnPrikaziKazne";
            this.btnPrikaziKazne.Size = new System.Drawing.Size(150, 81);
            this.btnPrikaziKazne.TabIndex = 11;
            this.btnPrikaziKazne.Text = "Prikazi SAOBRACAJNE KAZNE koje je radnik dodelio";
            this.btnPrikaziKazne.UseVisualStyleBackColor = true;
            this.btnPrikaziKazne.Click += new System.EventHandler(this.btnPrikaziKazne_Click);
            // 
            // btnPrikaziRegistracije
            // 
            this.btnPrikaziRegistracije.Location = new System.Drawing.Point(420, 191);
            this.btnPrikaziRegistracije.Name = "btnPrikaziRegistracije";
            this.btnPrikaziRegistracije.Size = new System.Drawing.Size(150, 81);
            this.btnPrikaziRegistracije.TabIndex = 10;
            this.btnPrikaziRegistracije.Text = "Prikazi REGISTRACIJE VOZILA koje je radnik dodelio";
            this.btnPrikaziRegistracije.UseVisualStyleBackColor = true;
            this.btnPrikaziRegistracije.Click += new System.EventHandler(this.btnPrikaziRegistracije_Click);
            // 
            // btnPrikaziPasose
            // 
            this.btnPrikaziPasose.Location = new System.Drawing.Point(217, 191);
            this.btnPrikaziPasose.Name = "btnPrikaziPasose";
            this.btnPrikaziPasose.Size = new System.Drawing.Size(150, 81);
            this.btnPrikaziPasose.TabIndex = 9;
            this.btnPrikaziPasose.Text = "Prikazi PASOSE koje je radnik dodelio";
            this.btnPrikaziPasose.UseVisualStyleBackColor = true;
            this.btnPrikaziPasose.Click += new System.EventHandler(this.btnPrikaziPasose_Click);
            // 
            // btnPrikaziLicneKarte
            // 
            this.btnPrikaziLicneKarte.Location = new System.Drawing.Point(15, 191);
            this.btnPrikaziLicneKarte.Name = "btnPrikaziLicneKarte";
            this.btnPrikaziLicneKarte.Size = new System.Drawing.Size(150, 81);
            this.btnPrikaziLicneKarte.TabIndex = 8;
            this.btnPrikaziLicneKarte.Text = "Prikazi LICNE KARTE koje je radnik dodelio";
            this.btnPrikaziLicneKarte.UseVisualStyleBackColor = true;
            this.btnPrikaziLicneKarte.Click += new System.EventHandler(this.btnPrikaziLicneKarte_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(636, 128);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(349, 50);
            this.btnObrisi.TabIndex = 15;
            this.btnObrisi.Text = "Obrisi radnika";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(636, 72);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(349, 50);
            this.btnIzmeni.TabIndex = 14;
            this.btnIzmeni.Text = "Izmeni radnika";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(636, 16);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(349, 50);
            this.btnDodaj.TabIndex = 13;
            this.btnDodaj.Text = "Dodaj radnika";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnPoDatumuRodjenja
            // 
            this.btnPoDatumuRodjenja.Location = new System.Drawing.Point(183, 104);
            this.btnPoDatumuRodjenja.Name = "btnPoDatumuRodjenja";
            this.btnPoDatumuRodjenja.Size = new System.Drawing.Size(242, 32);
            this.btnPoDatumuRodjenja.TabIndex = 5;
            this.btnPoDatumuRodjenja.Text = "Pretrazi radnike po datumu rodjenja";
            this.btnPoDatumuRodjenja.UseVisualStyleBackColor = true;
            this.btnPoDatumuRodjenja.Click += new System.EventHandler(this.btnPoDatumuRodjenja_Click);
            // 
            // btnPoImenu
            // 
            this.btnPoImenu.Location = new System.Drawing.Point(183, 16);
            this.btnPoImenu.Name = "btnPoImenu";
            this.btnPoImenu.Size = new System.Drawing.Size(242, 32);
            this.btnPoImenu.TabIndex = 1;
            this.btnPoImenu.Text = "Pretrazi radnike po imenu";
            this.btnPoImenu.UseVisualStyleBackColor = true;
            this.btnPoImenu.Click += new System.EventHandler(this.btnPoImenu_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPoSmeni);
            this.groupBox3.Controls.Add(this.txbSmena);
            this.groupBox3.Controls.Add(this.btnPrikaziVozacke);
            this.groupBox3.Controls.Add(this.btnPrikaziKazne);
            this.groupBox3.Controls.Add(this.btnPrikaziRegistracije);
            this.groupBox3.Controls.Add(this.btnPrikaziPasose);
            this.groupBox3.Controls.Add(this.btnPrikaziLicneKarte);
            this.groupBox3.Controls.Add(this.btnObrisi);
            this.groupBox3.Controls.Add(this.btnIzmeni);
            this.groupBox3.Controls.Add(this.btnDodaj);
            this.groupBox3.Controls.Add(this.btnPoDatumuRodjenja);
            this.groupBox3.Controls.Add(this.btnPoJMBG);
            this.groupBox3.Controls.Add(this.btnPoImenu);
            this.groupBox3.Controls.Add(this.txbDatumRodjenja);
            this.groupBox3.Controls.Add(this.txbIme);
            this.groupBox3.Controls.Add(this.txbJmbg);
            this.groupBox3.Location = new System.Drawing.Point(13, 331);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1001, 281);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            // 
            // btnPoSmeni
            // 
            this.btnPoSmeni.Location = new System.Drawing.Point(183, 148);
            this.btnPoSmeni.Name = "btnPoSmeni";
            this.btnPoSmeni.Size = new System.Drawing.Size(242, 32);
            this.btnPoSmeni.TabIndex = 7;
            this.btnPoSmeni.Text = "Pretrazi radnike po smeni";
            this.btnPoSmeni.UseVisualStyleBackColor = true;
            this.btnPoSmeni.Click += new System.EventHandler(this.btnPoSmeni_Click);
            // 
            // txbSmena
            // 
            this.txbSmena.Location = new System.Drawing.Point(16, 153);
            this.txbSmena.Name = "txbSmena";
            this.txbSmena.Size = new System.Drawing.Size(149, 22);
            this.txbSmena.TabIndex = 6;
            // 
            // btnPoJMBG
            // 
            this.btnPoJMBG.Location = new System.Drawing.Point(183, 59);
            this.btnPoJMBG.Name = "btnPoJMBG";
            this.btnPoJMBG.Size = new System.Drawing.Size(242, 32);
            this.btnPoJMBG.TabIndex = 3;
            this.btnPoJMBG.Text = "Pretrazi radnike po JMBG-u";
            this.btnPoJMBG.UseVisualStyleBackColor = true;
            this.btnPoJMBG.Click += new System.EventHandler(this.btnPoJMBG_Click);
            // 
            // txbDatumRodjenja
            // 
            this.txbDatumRodjenja.Location = new System.Drawing.Point(16, 109);
            this.txbDatumRodjenja.Name = "txbDatumRodjenja";
            this.txbDatumRodjenja.Size = new System.Drawing.Size(149, 22);
            this.txbDatumRodjenja.TabIndex = 4;
            // 
            // txbIme
            // 
            this.txbIme.Location = new System.Drawing.Point(16, 21);
            this.txbIme.Name = "txbIme";
            this.txbIme.Size = new System.Drawing.Size(149, 22);
            this.txbIme.TabIndex = 0;
            // 
            // txbJmbg
            // 
            this.txbJmbg.Location = new System.Drawing.Point(16, 64);
            this.txbJmbg.Name = "txbJmbg";
            this.txbJmbg.Size = new System.Drawing.Size(149, 22);
            this.txbJmbg.TabIndex = 2;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Smena";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 125;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Datum rodjenja";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 125;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "JMBG";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 120;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Ime";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader1.Width = 120;
            // 
            // radnici
            // 
            this.radnici.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.radnici.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radnici.FullRowSelect = true;
            this.radnici.GridLines = true;
            this.radnici.Location = new System.Drawing.Point(4, 19);
            this.radnici.Margin = new System.Windows.Forms.Padding(4);
            this.radnici.Name = "radnici";
            this.radnici.Size = new System.Drawing.Size(993, 173);
            this.radnici.TabIndex = 1;
            this.radnici.UseCompatibleStateImageBehavior = false;
            this.radnici.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Prezime";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 120;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radnici);
            this.groupBox2.Location = new System.Drawing.Point(13, 128);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1001, 196);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista radnika";
            // 
            // btnSaP
            // 
            this.btnSaP.Location = new System.Drawing.Point(213, 21);
            this.btnSaP.Name = "btnSaP";
            this.btnSaP.Size = new System.Drawing.Size(167, 81);
            this.btnSaP.TabIndex = 1;
            this.btnSaP.Text = "Svi radnici sa saltera \"PASOSI\"";
            this.btnSaP.UseVisualStyleBackColor = true;
            this.btnSaP.Click += new System.EventHandler(this.btnSaP_Click);
            // 
            // btnSaRV
            // 
            this.btnSaRV.Location = new System.Drawing.Point(416, 21);
            this.btnSaRV.Name = "btnSaRV";
            this.btnSaRV.Size = new System.Drawing.Size(167, 81);
            this.btnSaRV.TabIndex = 2;
            this.btnSaRV.Text = "Svi radnici sa saltera \"REGISTRACIJE VOZILA\"";
            this.btnSaRV.UseVisualStyleBackColor = true;
            this.btnSaRV.Click += new System.EventHandler(this.btnSaRV_Click);
            // 
            // btnSaSK
            // 
            this.btnSaSK.Location = new System.Drawing.Point(615, 21);
            this.btnSaSK.Name = "btnSaSK";
            this.btnSaSK.Size = new System.Drawing.Size(167, 81);
            this.btnSaSK.TabIndex = 3;
            this.btnSaSK.Text = "Svi radnici sa saltera \"SAOBRACAJNE KAZNE\"";
            this.btnSaSK.UseVisualStyleBackColor = true;
            this.btnSaSK.Click += new System.EventHandler(this.btnSaSK_Click);
            // 
            // btnSaVD
            // 
            this.btnSaVD.Location = new System.Drawing.Point(819, 21);
            this.btnSaVD.Name = "btnSaVD";
            this.btnSaVD.Size = new System.Drawing.Size(167, 81);
            this.btnSaVD.TabIndex = 4;
            this.btnSaVD.Text = "Svi radnici sa saltera \"VOZACKE DOZVOLE\"";
            this.btnSaVD.UseVisualStyleBackColor = true;
            this.btnSaVD.Click += new System.EventHandler(this.btnSaVD_Click);
            // 
            // btnSaLK
            // 
            this.btnSaLK.Location = new System.Drawing.Point(16, 21);
            this.btnSaLK.Name = "btnSaLK";
            this.btnSaLK.Size = new System.Drawing.Size(167, 81);
            this.btnSaLK.TabIndex = 0;
            this.btnSaLK.Text = "Svi radnici sa saltera \"LICNE KARTE\"";
            this.btnSaLK.UseVisualStyleBackColor = true;
            this.btnSaLK.Click += new System.EventHandler(this.btnSaLK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSaLK);
            this.groupBox1.Controls.Add(this.btnSaP);
            this.groupBox1.Controls.Add(this.btnSaRV);
            this.groupBox1.Controls.Add(this.btnSaSK);
            this.groupBox1.Controls.Add(this.btnSaVD);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1002, 118);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // FormRadnici
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 623);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(1040, 670);
            this.MinimumSize = new System.Drawing.Size(1040, 670);
            this.Name = "FormRadnici";
            this.Text = "RADNICI";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrikaziVozacke;
        private System.Windows.Forms.Button btnPrikaziKazne;
        private System.Windows.Forms.Button btnPrikaziRegistracije;
        private System.Windows.Forms.Button btnPrikaziPasose;
        private System.Windows.Forms.Button btnPrikaziLicneKarte;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnPoDatumuRodjenja;
        private System.Windows.Forms.Button btnPoImenu;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnPoJMBG;
        private System.Windows.Forms.TextBox txbDatumRodjenja;
        private System.Windows.Forms.TextBox txbIme;
        private System.Windows.Forms.TextBox txbJmbg;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView radnici;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSaP;
        private System.Windows.Forms.Button btnSaRV;
        private System.Windows.Forms.Button btnSaSK;
        private System.Windows.Forms.Button btnSaVD;
        private System.Windows.Forms.Button btnSaLK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPoSmeni;
        private System.Windows.Forms.TextBox txbSmena;
    }
}