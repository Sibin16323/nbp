﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormSaobracajneKazne : Form
    {
        public GraphClient client;
        public FormSaobracajneKazne()
        {
            InitializeComponent();
        }

        private void btnPoDatumu_Click(object sender, EventArgs e)
        {
            if (txbDatum.Text == "")
            {
                MessageBox.Show("Niste uneli datum saobracajne kazne!");
                return;
            }

            string datum = ".*" + txbDatum.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datum", datum);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:SaobracajnaKazna) and exists(n.datumKazne) and n.datumKazne =~ {datum} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<SaobracajnaKazna> sveKazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();
            
            this.saobracajneKazne.Items.Clear();

            foreach (SaobracajnaKazna sk in sveKazne)
            {
                ListViewItem item = new ListViewItem(new string[] { sk.datumKazne, sk.razlog, sk.cena });
                this.saobracajneKazne.Items.Add(item);
            }

            this.saobracajneKazne.Refresh();
        }

        private void btnPoRazlogu_Click(object sender, EventArgs e)
        {
            if (txbRazlog.Text == "")
            {
                MessageBox.Show("Niste uneli razlog saobracajne kazne!");
                return;
            }

            string razlog = ".*" + txbRazlog.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("razlog", razlog);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:SaobracajnaKazna) and exists(n.razlog) and n.razlog =~ {razlog} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<SaobracajnaKazna> sveKazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

            this.saobracajneKazne.Items.Clear();

            foreach (SaobracajnaKazna sk in sveKazne)
            {
                ListViewItem item = new ListViewItem(new string[] { sk.datumKazne, sk.razlog, sk.cena });
                this.saobracajneKazne.Items.Add(item);
            }

            this.saobracajneKazne.Refresh();
        }

        private void btnPoCeni_Click(object sender, EventArgs e)
        {
            if (txbCena.Text == "")
            {
                MessageBox.Show("Niste uneli cenu saobracajne kazne!");
                return;
            }

            string cena = ".*" + txbCena.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("cena", cena);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:SaobracajnaKazna) and exists(n.cena) and n.cena =~ {cena} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<SaobracajnaKazna> sveKazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

            this.saobracajneKazne.Items.Clear();

            foreach (SaobracajnaKazna sk in sveKazne)
            {
                ListViewItem item = new ListViewItem(new string[] { sk.datumKazne, sk.razlog, sk.cena });
                this.saobracajneKazne.Items.Add(item);
            }

            this.saobracajneKazne.Refresh();
        }

        private void btnSveSaobracajneKazne_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:SaobracajnaKazna) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<SaobracajnaKazna> sveKazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

            this.saobracajneKazne.Items.Clear();

            foreach (SaobracajnaKazna sk in sveKazne)
            {
                ListViewItem item = new ListViewItem(new string[] { sk.datumKazne, sk.razlog, sk.cena });
                this.saobracajneKazne.Items.Add(item);
            }

            this.saobracajneKazne.Refresh();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormDodajSaobracajnuKaznu DodajSaobracajnuKaznuForm = new FormDodajSaobracajnuKaznu();
            DodajSaobracajnuKaznuForm.client = client;
            DodajSaobracajnuKaznuForm.ShowDialog();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (saobracajneKazne.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite saobracajnu kaznu koju zelite da obrisete!");
                return;
            }

            string datumKazne = saobracajneKazne.SelectedItems[0].SubItems[0].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datumKazne", datumKazne);

            // PRVO BRISEMO RELACIJE
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (a)-[r]-(b:SaobracajnaKazna{datumKazne:{datumKazne}}) delete r",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            // DRUGO BRISEMO SAOBRACAJNU KAZNU
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:SaobracajnaKazna{datumKazne:{datumKazne}}) delete n",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            MessageBox.Show("Saobracajna kazna je uspesno obrisana sa svim njenim relacijama!");
        }

        private void FormSaobracajneKazne_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:SaobracajnaKazna) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<SaobracajnaKazna> sveKazne = ((IRawGraphClient)client).ExecuteGetCypherResults<SaobracajnaKazna>(query).ToList();

            this.saobracajneKazne.Items.Clear();

            foreach (SaobracajnaKazna sk in sveKazne)
            {
                ListViewItem item = new ListViewItem(new string[] { sk.datumKazne, sk.razlog, sk.cena });
                this.saobracajneKazne.Items.Add(item);
            }

            this.saobracajneKazne.Refresh();
        }
    }
}
