﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Neo4J_Repository.DomainModel;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Neo4J_Repository
{
    public partial class FormPasosi : Form
    {
        public GraphClient client;
        public FormPasosi()
        {
            InitializeComponent();
        }

        private void btnPoBrojuP_Click(object sender, EventArgs e)
        {
            if (txbBrojP.Text == "")
            {
                MessageBox.Show("Niste uneli broj pasosa!");
                return;
            }

            string brojPasosa = ".*" + txbBrojP.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojP", brojPasosa);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Pasos) and exists(n.brojPasosa) and n.brojPasosa =~ {brojP} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnPoMestuIzdavanja_Click(object sender, EventArgs e)
        {
            if (txbMestoIzdavanja.Text == "")
            {
                MessageBox.Show("Niste uneli mesto izdavanja pasosa!");
                return;
            }

            string mestoIzdavanja = ".*" + txbMestoIzdavanja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("mestoIzdavanja", mestoIzdavanja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Pasos) and exists(n.izdatOd) and n.izdatOd =~ {mestoIzdavanja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnPoDatumuVazenja_Click(object sender, EventArgs e)
        {
            if (txbDatumVazenja.Text == "")
            {
                MessageBox.Show("Niste uneli datum vazenja pasosa!");
                return;
            }

            string datumVazenja = ".*" + txbDatumVazenja.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("datumVazenja", datumVazenja);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Pasos) and exists(n.vaziDo) and n.vaziDo =~ {datumVazenja} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnPoPolu_Click(object sender, EventArgs e)
        {
            if (txbPol.Text == "")
            {
                MessageBox.Show("Niste uneli pol!");
                return;
            }

            if (txbPol.Text != "M" && txbPol.Text != "Z")
            {
                MessageBox.Show("Validan unos je samo 'M' ili 'Z' (velika slova)!");
                return;
            }

            string pol = ".*" + txbPol.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("pol", pol);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Pasos) and exists(n.pol) and n.pol =~ {pol} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnPoDrzavljanstvu_Click(object sender, EventArgs e)
        {
            if (txbDrzavljanstvo.Text == "")
            {
                MessageBox.Show("Niste uneli drzavljanstvo!");
                return;
            }

            string drzavljanstvo = ".*" + txbDrzavljanstvo.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("drzavljanstvo", drzavljanstvo);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Pasos) and exists(n.drzavljanstvo) and n.drzavljanstvo =~ {drzavljanstvo} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnPoPrebivalistu_Click(object sender, EventArgs e)
        {
            if (txbPrebivaliste.Text == "")
            {
                MessageBox.Show("Niste uneli prebivaliste!");
                return;
            }

            string prebivaliste = ".*" + txbPrebivaliste.Text + ".*";

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("prebivaliste", prebivaliste);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Pasos) and exists(n.prebivaliste) and n.prebivaliste =~ {prebivaliste} return n",
                                                                    queryDict, CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnSviPasosi_Click(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Pasos) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }

        private void btnDodajPasos_Click(object sender, EventArgs e)
        {
            FormDodajPasos DodajPasosForm = new FormDodajPasos();
            DodajPasosForm.client = client;
            DodajPasosForm.ShowDialog();
        }

        private void btnObrisiPasos_Click(object sender, EventArgs e)
        {
            if (pasosi.SelectedItems.Count == 0)
            {
                MessageBox.Show("Izaberite pasos koji zelite da obrisete!");
                return;
            }

            string brojPasosa = pasosi.SelectedItems[0].SubItems[0].Text;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("brojPasosa", brojPasosa);

            // PRVO BRISEMO RELACIJE
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (a)-[r]-(b:Pasos{brojPasosa:{brojPasosa}}) delete r",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query);

            // DRUGO BRISEMO PASOS
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Pasos{brojPasosa:{brojPasosa}}) delete n",
                                                                    queryDict, CypherResultMode.Set);

            ((IRawGraphClient)client).ExecuteCypher(query2);

            MessageBox.Show("Pasos je uspesno obrisan sa svim njegovim relacijama!");
        }

        private void FormPasosi_Load(object sender, EventArgs e)
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match (n:Pasos) return n",
                                                           new Dictionary<string, object>(), CypherResultMode.Set);

            List<Pasos> sviPasosi = ((IRawGraphClient)client).ExecuteGetCypherResults<Pasos>(query).ToList();

            this.pasosi.Items.Clear();

            foreach (Pasos p in sviPasosi)
            {
                ListViewItem item = new ListViewItem(new string[] { p.brojPasosa, p.izdatOd, p.vaziOd, p.vaziDo, p.pol, p.drzavljanstvo, p.prebivaliste });
                this.pasosi.Items.Add(item);
            }

            this.pasosi.Refresh();
        }
    }
}
