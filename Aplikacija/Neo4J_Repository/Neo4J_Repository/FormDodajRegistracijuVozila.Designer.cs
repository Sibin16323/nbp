﻿namespace Neo4J_Repository
{
    partial class FormDodajRegistracijuVozila
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.txbUnosKategorije = new System.Windows.Forms.TextBox();
            this.cbxKorisnici = new System.Windows.Forms.ComboBox();
            this.cbxRadnici = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDatumDo = new System.Windows.Forms.DateTimePicker();
            this.txbUnosBroj = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 17);
            this.label8.TabIndex = 27;
            this.label8.Text = "Unesite kategoriju:";
            // 
            // txbUnosKategorije
            // 
            this.txbUnosKategorije.Location = new System.Drawing.Point(188, 211);
            this.txbUnosKategorije.MaxLength = 100;
            this.txbUnosKategorije.Name = "txbUnosKategorije";
            this.txbUnosKategorije.Size = new System.Drawing.Size(100, 22);
            this.txbUnosKategorije.TabIndex = 4;
            // 
            // cbxKorisnici
            // 
            this.cbxKorisnici.FormattingEnabled = true;
            this.cbxKorisnici.Location = new System.Drawing.Point(188, 80);
            this.cbxKorisnici.Name = "cbxKorisnici";
            this.cbxKorisnici.Size = new System.Drawing.Size(301, 24);
            this.cbxKorisnici.TabIndex = 1;
            // 
            // cbxRadnici
            // 
            this.cbxRadnici.FormattingEnabled = true;
            this.cbxRadnici.Location = new System.Drawing.Point(188, 37);
            this.cbxRadnici.Name = "cbxRadnici";
            this.cbxRadnici.Size = new System.Drawing.Size(301, 24);
            this.cbxRadnici.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Izaberite radnika:";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(380, 124);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(109, 107);
            this.btnDodaj.TabIndex = 5;
            this.btnDodaj.Text = "Dodaj registraciju";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Izaberite korisnika:";
            // 
            // dtpDatumDo
            // 
            this.dtpDatumDo.CustomFormat = "dd.MM.yyyy.";
            this.dtpDatumDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumDo.Location = new System.Drawing.Point(188, 166);
            this.dtpDatumDo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDatumDo.Name = "dtpDatumDo";
            this.dtpDatumDo.Size = new System.Drawing.Size(160, 22);
            this.dtpDatumDo.TabIndex = 3;
            // 
            // txbUnosBroj
            // 
            this.txbUnosBroj.Location = new System.Drawing.Point(188, 121);
            this.txbUnosBroj.MaxLength = 9;
            this.txbUnosBroj.Name = "txbUnosBroj";
            this.txbUnosBroj.Size = new System.Drawing.Size(100, 22);
            this.txbUnosBroj.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Unesite registarski broj:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Unesite datum vazenja:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txbUnosKategorije);
            this.groupBox1.Controls.Add(this.cbxKorisnici);
            this.groupBox1.Controls.Add(this.cbxRadnici);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDodaj);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpDatumDo);
            this.groupBox1.Controls.Add(this.txbUnosBroj);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(507, 246);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodavanje registracije vozila";
            // 
            // FormDodajRegistracijuVozila
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 268);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(550, 315);
            this.MinimumSize = new System.Drawing.Size(550, 315);
            this.Name = "FormDodajRegistracijuVozila";
            this.Text = "DODAVANJE REGISTRACIJE VOZILA";
            this.Load += new System.EventHandler(this.FormDodajRegistracijuVozila_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbUnosKategorije;
        private System.Windows.Forms.ComboBox cbxKorisnici;
        private System.Windows.Forms.ComboBox cbxRadnici;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDatumDo;
        private System.Windows.Forms.TextBox txbUnosBroj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}